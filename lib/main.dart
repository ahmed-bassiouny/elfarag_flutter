import 'package:easy_localization/easy_localization_delegate.dart';
import 'package:elfarag/model/contact.dart';
import 'package:elfarag/screen/aboutus_screen.dart';
import 'package:elfarag/screen/contact_us_screen.dart';
import 'package:elfarag/screen/driver/driver_home_screen.dart';
import 'package:elfarag/screen/driver/driver_trips_screen.dart';
import 'package:elfarag/screen/driver/select_service_type.dart';
import 'package:elfarag/screen/driver/update_driver_profile_screen.dart';
import 'package:elfarag/screen/driver/view_driver_profile_screen.dart';
import 'package:elfarag/screen/login_screen.dart';
import 'package:elfarag/screen/map_screen.dart';
import 'package:elfarag/screen/splash_screen.dart';
import 'package:elfarag/screen/user/create_trip.dart';
import 'package:elfarag/screen/user/track_driver.dart';
import 'package:elfarag/screen/user/update_user_profile_screen.dart';
import 'package:elfarag/screen/user/user_home.dart';
import 'package:elfarag/screen/user/user_trips_screen.dart';
import 'package:elfarag/screen/user_type_screen.dart';
import 'package:elfarag/screen/verify_phone_screen.dart';
import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:easy_localization/easy_localization.dart';

void main() => runApp(EasyLocalization(child: MyApp()));

class MyApp extends StatelessWidget {
  // This widget is the root of your application.

  static var data;
  static AppLocalizations app;


  @override
  Widget build(BuildContext context) {
   data = EasyLocalizationProvider.of(context).data;
    return EasyLocalizationProvider(
      data: data,
      child: MaterialApp(
        title: '',
        
        localizationsDelegates: [
          GlobalMaterialLocalizations.delegate,
          GlobalWidgetsLocalizations.delegate,
          //app-specific localization
          EasylocaLizationDelegate(locale: data.locale, path: 'lang'),
        ],
        supportedLocales: [Locale('en', 'US'), Locale('ar', 'AR')],
        locale: data.savedLocale,
        theme: ThemeData(
          fontFamily: "cairo" ,
          // This is the theme of your application.
          //
          // Try running your application with "flutter run". You'll see the
          // application has a blue toolbar. Then, without quitting the app, try
          // changing the primarySwatch below to Colors.green and then invoke
          // "hot reload" (press "r" in the console where you ran "flutter run",
          // or simply save your changes to "hot reload" in a Flutter IDE).
          // Notice that the counter didn't reset back to zero; the application
          // is not restarted.
          primarySwatch: Colors.green,
        ),
        home: SplashScreen(),
        debugShowCheckedModeBanner: false,
        routes: {
          "/splash": (_) => SplashScreen(),
          "/login": (_) => LoginScreen(),
          "/verify": (_) => VerifyPhoneScreen(),
          "/complete_user_profile": (_) => UpdateUserProfileScreen(),
          "/complete_driver_profile": (_) => UpdateDriverProfileScreen(),
          "/view_driver_profile": (_) => ViewDriverProfileScreen(),
          "/user_type": (_) => UserTypeScreen(),
          "/user_home": (_) => UserHome(),
          "/driver_home": (_) => DriverHomeScreen(),
          "/service_type": (_) => SelectServiceType(),
          "/map": (_) => MapScreen(),
          "/create_trip": (_) => CreateTrip(),
          "/contact": (_) => ContactUsScreen(),
          "/driver_trips": (_) => DriverTripsScreen(),
          "/user_trips": (_) => UserTripsScreen(),
        },
        onGenerateRoute: (RouteSettings settings) {
          if (settings.name == '/track_driver') {
            return MaterialPageRoute(builder: (_) {
              Map map = Map.from(settings.arguments);
              return TrackDriver(
                currentUserKey: map['id'],
              );
            });
          } else if (settings.name == '/about' || settings.name == '/terms') {
            return MaterialPageRoute(builder: (_) {
              Map map = Map.from(settings.arguments);
              return AboutUsScreen(
                url: map['url'],
              );
            });
          }
        },
      ),
    );
  }
}
