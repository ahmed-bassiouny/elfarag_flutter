class TripStatus {
  static int created =  0;
  static final int cancelByUser =  1;
  static final int cancelByDriver =  2;
  
  // first type
  // trip from and to point
  static final int goToClient = 7;
  static final int startTrip =  3;
  static final int endTrip =  4;

  // // second type
  // // trip driver location and from point
  // static final int goToClientFromMyPoint =  5;
  // static final int finish =  6;
  
}