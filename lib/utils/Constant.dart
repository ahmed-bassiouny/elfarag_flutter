class Constant {
  static final kGoogleApiKey = "AIzaSyANjyY8gvJRXL84lNXzlnfHZ3P_KTihXTI";
  static final USER_KEY = "user";
  static final DRIVER_KEY = "driver";
  static final USERS_KEY = "users";
  static final PHONE_KEY = "phone";
  static final EMAIL_KEY = "email";
  static final NAME_KEY = "name";
  static final PHOTO_KEY = "photo";
  static final USER_TYPE_KEY = "user_type";
  static final BLOCK_KEY = "block";
  static final ACCESS_KEY = "access";
  static final CITY_KEY = "city";
  static final CAR_MODEL_KEY = "car_model";
  static final CAR_LICENSE_URL_KEY = "car_license_url";
  static final CAR_TYPE_KEY = "car_type";
  static final CAR_NUMBER_KEY = "car_number";
  static final IBAN_NUMBER_KEY = "iban_number";
  static final PERSONAL_NUMBER_KEY =  "personal_number";
  static final PERSONAL_NUMBER_URL_KEY =  "personal_number_url";
  static final INSURANCE_URL_KEY =  "insurance_url";
  static final LAT_FROM_KEY =  "lat_from";
  static final LNG_FROM_KEY =  "lng_from";
  static final ADDRESS_FROM_KEY =  "address_from";
  static final LAT_TO_KEY =  "lat_to";
  static final LNG_TO_KEY =  "lng_to";
  static final ADDRESS_TO_KEY =  "address_to";
  static final TIME_KEY =  "time";
  static final OPEN_KEY =  "open";
  static final CURRENT_TRIP_KEY =  "current_trip";
  static final TRIPS_KEY =  "trips";
  static final TRIP_TYPE_KEY =  "trip_type";
  static final SERVICES_KEY = "services";
  static final IS_ONLINE_KEY = "is_online";
  static final SUBSCRIPE = "subscripe";
  static final LAT = "lat";
  static final LNG = "lng";
  static final USER_ID_KEY = "user_id";
  static final DRIVER_ID_KEY = "driver_id";
  static final DRIVER_LICENSE_URL_KEY = "driver_license_url";
  static final TRIP_STATUS = "trip_status";
  static final CREATED_AT = "create_at";
  static final NOTE = "note";
  static final PRICE = "price";

  static final PENDING_ACCOUNT =
      "Your Account in Review \n Thanks for joined to our team";
  static final BLOCK_ACCOUNT = "Your Account blocked , please contact us";
  static final TRIP_MANAGER = "trips_managers";

  static final String BASE_URL= "https://alfaraj.sa/elfarag/";

  static final Map<int, bool> servicesForDriver = {
    2: false,
    3: false,
    4: false,
    5: false,
    6: false,
    7: false,
    8: false,
    9: false,
    10: false,
    11: false,
  };
  static final Map<int, String> servicesForDriverString = {
    2: 'regular_wash',
    3: 'hydraulic_winch',
    4: 'pertol_delivery_inside_city',
    5: 'pertol_delivery_outside_city',
    6: 'battery_charge_inside_city',
    7: 'battery_charge_outside_city',
    8: 'battery_change',
    9: 'change_tire',
    10:'spare_tire_fill_air',
    11:'open_car_inside_city',
  };

  static final Map<int, String> servicesForUserString = {
    2: 'regular_wash',
    3: 'hydraulic_winch',
    4: 'pertol_delivery_inside_city',
    5: 'pertol_delivery_outside_city',
    6: 'battery_charge_inside_city',
    7: 'battery_charge_outside_city',
    8: 'battery_change',
    9: 'change_tire',
    10:'spare_tire_fill_air',
    11:'open_car_inside_city',
    12:'open_car_outside_city',
    13: 'logistics_between_cities',
    14:'taxi_service',
  };
}
