import 'package:easy_localization/easy_localization.dart';
import 'package:elfarag/utils/MyColor.dart';
import 'package:elfarag/utils/Utils.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class VerifyPhoneScreen extends StatefulWidget {
  @override
  _VerifyPhoneScreenState createState() => _VerifyPhoneScreenState();
}

class _VerifyPhoneScreenState extends State<VerifyPhoneScreen> {
  TextEditingController emailController = new TextEditingController();

  @override
  void dispose() {
    emailController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    var app = AppLocalizations.of(context);
    final args = ModalRoute.of(context).settings.arguments;
    print(args);

    final emailField = TextField(
      obscureText: false,
      controller: emailController,
      keyboardType: TextInputType.number,
      decoration: InputDecoration(
        contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
        hintText: app.tr('code'),
      ),
    );

    final loginButon = Material(
        elevation: 5.0,
        borderRadius: BorderRadius.circular(30.0),
        color: Color(0xff01A0C7),
        child: MaterialButton(
          minWidth: MediaQuery.of(context).size.width,
          padding: EdgeInsets.fromLTRB(10.0, 10.0, 10.0, 10.0),
          onPressed: () {
            //emailController.text
            // passwordController.text
            if (emailController.text.length == 4) {
              Navigator.pushNamed(context, '/user_type');
            } else {
              Utils.showMyDialog(context, app.tr('error'), app.tr('invalid_code'));
            }
          },
          child: Text(
            app.tr('verify'),
            textAlign: TextAlign.center,
            style: TextStyle(color: Colors.white),
          ),
        ));

    return Scaffold(
        appBar: AppBar(
          title: Text(app.tr('verify_code')),
          backgroundColor: MyColor.green,
        ),
        body: GestureDetector(
          onTap: () {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          child: ListView(
            children: <Widget>[
              Container(
                color: Colors.white,
                child: Padding(
                  padding: const EdgeInsets.all(36.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      SizedBox(
                        height: 150.0,
                        width: 150,
                        // child: Image.asset(
                        //   "assets/logo.png",
                        //   fit: BoxFit.contain,
                        // ),
                        child: CircleAvatar(
                            backgroundImage: AssetImage("images/logo.png"),
                            backgroundColor: Colors.white),
                      ),
                      SizedBox(height: 45.0),
                      emailField,
                      SizedBox(height: 45.0),
                      SizedBox(
                        height: 50.0,
                        width: 150,
                        child: loginButon,
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ));
  }
}
