import 'package:easy_localization/easy_localization.dart';
import 'package:elfarag/utils/Constant.dart';
import 'package:elfarag/utils/trip_status.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/material.dart';
import 'package:toast/toast.dart';

class UserTripsScreen extends StatefulWidget {
  @override
  _UserTripsScreenState createState() => _UserTripsScreenState();
}

class _UserTripsScreenState extends State<UserTripsScreen> {
  var list = [];
  bool isLoading = true;

  String getStatusStr(int status) {
    if (status == TripStatus.created) {
      return "trip_created";
    } else if (status == TripStatus.goToClient) {
      return "go_to_client";
    } else if (status == TripStatus.startTrip) {
      return "started";
    } else if (status == TripStatus.endTrip) {
      return "finished";
    } else if (status == TripStatus.cancelByDriver) {
      return "cancel_by_driver";
    } else if (status == TripStatus.cancelByUser) {
      return "cancel_by_you";
    } else {
      return "";
    }
  }

  initState() {
    super.initState();
    fetchData();
  }

  void fetchData() async {
    var userId = await FirebaseAuth.instance.currentUser();
    FirebaseDatabase.instance
        .reference()
        .child(Constant.TRIPS_KEY)
        .orderByChild(Constant.USER_ID_KEY)
        .equalTo(userId.uid)
        .once()
        .then((DataSnapshot onValue) {
      if (onValue.value != null) {
        onValue.value.forEach((key, value) {
          list.add(value);
        });
      }
      setState(() {
        isLoading = false;
      });
    }).catchError((onError) {
      Navigator.of(context).pop();
      print(onError.toString());
      Toast.show(onError.toString(), context);
    });
  }

  @override
  Widget build(BuildContext context) {
    var app = AppLocalizations.of(context);
    return Scaffold(
      appBar: AppBar(
        title: Text(app.tr('your_trips')),
      ),
      body: isLoading
          ? Center(
              child: CircularProgressIndicator(),
            )
          : ListView.separated(
              itemCount: list.length,
              separatorBuilder: (ctx, index) {
                return Divider();
              },
              itemBuilder: (ctx, index) {
                var date = new DateTime.fromMillisecondsSinceEpoch(
                    list[index][Constant.CREATED_AT]);
                return ListTile(
                  title: Text(app.tr(getStatusStr(list[index][Constant.TRIP_STATUS]))),
                  trailing: Text(list[index][Constant.PRICE] == null
                      ? "0"
                      : "${(list[index][Constant.PRICE]).round()} ${app.tr('sar')}"),
                  subtitle: Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: <Widget>[
                      Text("${app.tr('trip_type')} :  ${app.tr(Constant.servicesForUserString[list[index][Constant.TRIP_TYPE_KEY]])}"),
                      Text(
                          "${date.year}-${date.month}-${date.day} ${date.hour}:${date.minute}"),
                      Text(list[index][Constant.ADDRESS_FROM_KEY]),
                      Text(list[index][Constant.ADDRESS_TO_KEY]),
                    ],
                  ),
                );
              },
            ),
    );
  }
}
