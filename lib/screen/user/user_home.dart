import 'dart:async';

import 'package:dio/dio.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:elfarag/main.dart';
import 'package:elfarag/utils/Constant.dart';
import 'package:elfarag/utils/MyColor.dart';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/material.dart';
import 'package:share/share.dart';
import 'package:url_launcher/url_launcher.dart';

class UserHome extends StatefulWidget {
  @override
  _UserHomeState createState() => _UserHomeState();
}

class _UserHomeState extends State<UserHome> {
  bool loading = true;
  bool haveTrip = false;
  String fromAddress = "";
  String toAddress = "";
  String tripType = "";
  String note = "";
  String status = "";
  String driverId = "";
  StreamSubscription<Event> tripValues;
  var app;

  void fetchData() async {
    FirebaseUser currentUser = await FirebaseAuth.instance.currentUser();
    FirebaseDatabase.instance
        .reference()
        .child(Constant.USERS_KEY)
        .child(currentUser.uid)
        .onValue
        .listen((onValue) {
      String currentTrip = onValue.snapshot.value == null
          ? ""
          : onValue.snapshot.value[Constant.CURRENT_TRIP_KEY];
      setState(() {
        if (currentTrip == null || currentTrip.isEmpty) {
          haveTrip = false;
          loading = false;
        } else {
          trackTrip(currentTrip);
        }
      });
    }).onError((handleError) {
      print('herer');
      haveTrip = false;
      loading = false;
    });
  }

  void trackTrip(String tripId) {
    tripValues = FirebaseDatabase.instance
        .reference()
        .child(Constant.TRIPS_KEY)
        .child(tripId)
        .onValue
        .listen((onData) {
      fromAddress =
          "${app.tr('from')} : ${onData.snapshot.value[Constant.ADDRESS_FROM_KEY]}";
      toAddress =
          "${app.tr('to')} : ${onData.snapshot.value[Constant.ADDRESS_TO_KEY]}";
      tripType =
          "${app.tr('trip_type')} :  ${app.tr(Constant.servicesForUserString[onData.snapshot.value[Constant.TRIP_TYPE_KEY]])}";
      note = "${app.tr('note')} : ${onData.snapshot.value[Constant.NOTE]}";
      driverId = onData.snapshot.value[Constant.DRIVER_ID_KEY];
      switch (onData.snapshot.value[Constant.TRIP_STATUS]) {
        case 0:
          status = "${app.tr('trip_status')} : ${app.tr('trip_created')}";
          haveTrip = true;
          break;
        case 1:
        case 2:
        case 6:
          haveTrip = false;
          break;
        case 5:
        case 7:
          status = "${app.tr('trip_status')} : ${app.tr('on_the_way')}";
          haveTrip = true;
          break;
        case 3:
          status = "${app.tr('trip_status')} : ${app.tr('driver_start_point')}";
          haveTrip = true;
          break;
        case 4:
          status = "${app.tr('trip_status')} : ${app.tr('driver_end_point')}";
          haveTrip = true;
          break;
        default:
          status = "";
          break;
      }

      setState(() {
        loading = false;
      });
    });
  }

  @override
  void initState() {
    super.initState();
    fetchData();
  }

  @override
  void dispose() {
    tripValues.cancel();
    super.dispose();
    
  }

  @override
  Widget build(BuildContext context) {
    app = AppLocalizations.of(context);
    return Scaffold(
      backgroundColor: Colors.red,
      appBar: AppBar(
        title: Text(app.tr('home_title')),
      ),
      drawer: Drawer(
        child: ListView(
          // Important: Remove any padding from the ListView.
          padding: EdgeInsets.zero,
          children: <Widget>[
            DrawerHeader(
              child: Image(
                width: 100,
                height: 100,
                image: AssetImage("images/logo.png"),
              ),
              decoration: BoxDecoration(
                color: Colors.white,
              ),
            ),
            ListTile(
              title: Text(app.tr('your_trips')),
              leading: Icon(Icons.history),
              onTap: () {
                Navigator.pop(context);
                Navigator.pushNamed(context, '/user_trips');
              },
            ),
            ListTile(
              title: Text(app.tr('edit_profile')),
              leading: Icon(Icons.person),
              onTap: () {
                Navigator.pop(context);
                Navigator.pushNamed(context, '/complete_user_profile');
              },
            ),
            ListTile(
              title: Text(app.tr('about_us')),
              leading: Icon(Icons.info),
              onTap: () {
                var url = Localizations.localeOf(context).languageCode == "ar"
                    ? "https://alfaraj.sa/elfarag/web_site/web_site/about_arabic.html"
                    : "https://alfaraj.sa/elfarag/web_site/web_site/about.html";
                Navigator.pop(context);
                Navigator.pushNamed(context, '/about', arguments: {"url": url});
              },
            ),
            ListTile(
              title: Text(app.tr('terms')),
              leading: Icon(Icons.info),
              onTap: () {
                var url = Localizations.localeOf(context).languageCode == "ar"
                    ? "https://alfaraj.sa/elfarag/web_site/web_site/terms_arabic.html"
                    : "https://alfaraj.sa/elfarag/web_site/web_site/terms.html";
                Navigator.pop(context);
                Navigator.pushNamed(context, '/terms', arguments: {"url": url});
              },
            ),
            ListTile(
              title: Text(app.tr('contact')),
              leading: Icon(Icons.contact_phone),
              onTap: () {
                Navigator.pop(context);
                Navigator.pushNamed(context, '/contact', arguments: {
                  "url":
                      "https://alfaraj.sa/elfarag/web_site/web_site/contact.html"
                });
              },
            ),
            // ListTile(
            //   title: Text(app.tr('subscription')),
            //   leading: Icon(Icons.subscriptions),
            //   onTap: () async {
            //     var url =
            //         "https://play.google.com/store/apps/details?id=com.wAlFaraj_8732233";
            //     Navigator.pop(context);
            //     if (await canLaunch(url)) {
            //       await launch(url);
            //     } else {
            //       throw 'Could not launch ';
            //     }
            //   },
            // ),
            // ListTile(
            //   title: Text(app.tr('share_app')),
            //   leading: Icon(Icons.share),
            //   onTap: () {
            //     Navigator.pop(context);
            //     Share.share(
            //         'check farag app android : google.com , ios : apple.com');
            //   },
            // ),
            ListTile(
              title: Text(app.tr('change_lang')),
              leading: Icon(Icons.language),
              onTap: () {
                Navigator.pop(context);

                this.setState(() {
                  Localizations.localeOf(context).languageCode == "ar"
                      ? MyApp.data.changeLocale(Locale("en", "US"))
                      : MyApp.data.changeLocale(Locale("ar", "AR"));
                });
              },
            ),
            ListTile(
              title: Text(app.tr('logout')),
              leading: Icon(Icons.subdirectory_arrow_left),
              onTap: () {
                Navigator.pop(context);
                if (haveTrip) return;
                FirebaseAuth.instance.signOut();
                Navigator.of(context).pushNamedAndRemoveUntil(
                    '/splash', (Route<dynamic> route) => false);
              },
            ),
          ],
        ),
      ),
      body: loading
          ? CircularProgressIndicator()
          : Center(
              child: Container(
                padding: EdgeInsets.all(20),
                margin: EdgeInsets.all(20),
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.all(const Radius.circular(5.0)),
                ),
                child: haveTrip
                    ? Column(
                        crossAxisAlignment: CrossAxisAlignment.stretch,
                        mainAxisSize: MainAxisSize.min,
                        children: <Widget>[
                          Text(
                            app.tr('trip_details'),
                            textAlign: TextAlign.center,
                            style: TextStyle(color: Colors.black, fontSize: 20),
                          ),
                          Padding(
                            padding: EdgeInsets.only(top: 8),
                          ),
                          Text(fromAddress),
                          Padding(
                            padding: EdgeInsets.only(top: 8),
                          ),
                          Text(toAddress),
                          Padding(
                            padding: EdgeInsets.only(top: 8),
                          ),
                          Text(tripType),
                          Padding(
                            padding: EdgeInsets.only(top: 8),
                          ),
                          Text(note),
                          Padding(
                            padding: EdgeInsets.only(top: 8),
                          ),
                          Text(status, style: TextStyle(color: MyColor.green)),
                          Padding(
                            padding: EdgeInsets.only(top: 8),
                          ),
                          FlatButton(
                            child: Text(app.tr('view_driver_info')),
                            onPressed: () {
                              Navigator.of(context).pushNamed("/track_driver",
                                  arguments: {"id": driverId});
                            },
                          )
                        ],
                      )
                    : Text(app.tr('no_trip')),
              ),
            ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
      floatingActionButton: (loading || haveTrip)
          ? Container()
          : FloatingActionButton.extended(
              icon: Icon(Icons.add),
              onPressed: () {
                Navigator.of(context).pushNamed("/create_trip");
              },
              label: Text(app.tr('create_trip')),
            ),
    );
  }
}
