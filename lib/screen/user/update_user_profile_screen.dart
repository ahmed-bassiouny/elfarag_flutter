import 'dart:core';
import 'dart:io';
import 'package:async/async.dart';
import 'package:dio/dio.dart';
import 'package:easy_localization/easy_localization.dart';

import 'package:elfarag/utils/Constant.dart';
import 'package:elfarag/utils/Utils.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_native_image/flutter_native_image.dart';
import 'package:image_picker/image_picker.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:toast/toast.dart';
import 'package:http/http.dart' as http;

class UpdateUserProfileScreen extends StatefulWidget {
  @override
  _UpdateUserProfileScreenState createState() =>
      _UpdateUserProfileScreenState();
}

class _UpdateUserProfileScreenState extends State<UpdateUserProfileScreen> {
  File _image;
  String _name = "";
  String _email = "";
  String _photoUrl = "";
  bool loading = true;
  var user;

  final _formKey = GlobalKey<FormState>();
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  FirebaseUser currentUser;
  final emailController = TextEditingController();
  final nameController = TextEditingController();

  @override
  initState() {
    super.initState();
    fetchData();
  }

  fetchData() async {
    currentUser = await FirebaseAuth.instance.currentUser();
    Response response = await Dio().post("${Constant.BASE_URL}select_user.php",
        queryParameters: {"phone": currentUser.phoneNumber});
    if (response.statusCode == 200 && response.data['status']) {
      user = response.data['data'];
      if (user != null) {
        setState(() {
          nameController.text = user[Constant.NAME_KEY];
          emailController.text = user[Constant.EMAIL_KEY];
          _photoUrl = user[Constant.PHOTO_KEY];
          print(_photoUrl);
          loading = false;
        });
      } else {
        setState(() {
          loading = false;
        });
      }
    }
  }

  Future getImageGaller() async {
    var image = await ImagePicker.pickImage(source: ImageSource.gallery);

    setState(() {
      _image = image;
    });
  }

  Future getImageCamera() async {
    var image = await ImagePicker.pickImage(source: ImageSource.camera);

    setState(() {
      _image = image;
    });
  }

  void saveUser() async {
    if (_image == null && _photoUrl.isEmpty) {
      final snackBar = SnackBar(content: Text('Please Select Image'));

      _scaffoldKey.currentState.showSnackBar(snackBar);
    } else {
      setState(() {
        loading = true;
      });
      if (_image != null) {
        uploadPic();
      } else {
        saveUserInfo();
      }
    }
  }

  void saveUserInfo() async {
    Response response;
    if (user == null) {
      // create
      response = await Dio()
          .post("${Constant.BASE_URL}create_user.php", queryParameters: {
        Constant.EMAIL_KEY: _email,
        Constant.NAME_KEY: _name,
        Constant.PHONE_KEY: currentUser.phoneNumber,
        Constant.PHOTO_KEY: _photoUrl,
        Constant.USER_TYPE_KEY: Constant.USER_KEY,
        "firebase_key": currentUser.uid
      });
    } else {
      // update
      response = await Dio()
          .post("${Constant.BASE_URL}update_user.php", queryParameters: {
        Constant.EMAIL_KEY: _email,
        Constant.NAME_KEY: _name,
        Constant.PHOTO_KEY: _photoUrl,
        "firebase_key":currentUser.uid
      });
    }
    setState(() {
      loading = false;
    });
    print(response);
    if (response.statusCode == 200 && response.data['status']) {
      Navigator.of(context).pushNamedAndRemoveUntil(
          "/user_home", (Route<dynamic> route) => false);
    } else {
      Utils.showMyDialog(context, "error", 'invalid_input');
    }
  }

  Future uploadPic() async {
    if (currentUser == null) return;
    String url = 'https://alfaraj.sa/elfarag/upload.php';

    File compressedFile = await Utils.compress(_image);

    FormData formData = FormData.fromMap({
      "name": currentUser.uid,
      "avatar": await MultipartFile.fromFile(compressedFile.path,
          filename: _image.path.split("/").last)
    });
    var response = await Dio().post(url, data: formData);
    if (response.data['status']) {
      _photoUrl = response.data['url'];
      saveUserInfo();
    } else {
      setState(() {
        loading = false;
        Toast.show("Error", context);
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    var app = AppLocalizations.of(context);

    final emailField = TextFormField(
      obscureText: false,
      controller: emailController,
      validator: (String str) =>
          str.trim().contains('@') && str.trim().contains('.')
              ? null
              : app.tr('invalid_input'),
      onSaved: (String str) {
        this._email = str;
      },
      keyboardType: TextInputType.emailAddress,
      decoration: InputDecoration(
          contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
          hintText: app.tr('email'),
          border:
              OutlineInputBorder(borderRadius: BorderRadius.circular(32.0))),
    );

    final nameField = TextFormField(
      obscureText: false,
      controller: nameController,
      validator: (String str) =>
          str.trim().length < 5 ? app.tr('invalid_input') : null,
      onSaved: (String str) {
        this._name = str;
      },
      keyboardType: TextInputType.text,
      decoration: InputDecoration(
          contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
          hintText: app.tr('name'),
          border:
              OutlineInputBorder(borderRadius: BorderRadius.circular(32.0))),
    );

    final loginButon = Material(
        elevation: 1.0,
        borderRadius: BorderRadius.circular(30.0),
        color: Color(0xff01A0C7),
        child: MaterialButton(
          minWidth: MediaQuery.of(context).size.width,
          padding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
          onPressed: () {
            if (_formKey.currentState.validate()) {
              _formKey.currentState.save();
              saveUser();
            }
          },
          child: Text(
            app.tr('save'),
            textAlign: TextAlign.center,
            style: TextStyle(color: Colors.white),
          ),
        ));

    final selectImage = FlatButton(
      onPressed: () {
        showDialog(
          context: context,
          builder: (BuildContext context) {
            // return object of type Dialog
            return AlertDialog(
              title: new Text(app.tr('change_image')),
              content: new Text(app.tr('select_image')),
              actions: <Widget>[
                // usually buttons at the bottom of the dialog
                new FlatButton(
                  child: new Text(app.tr('gallery')),
                  onPressed: () {
                    Navigator.of(context).pop();
                    getImageGaller();
                  },
                ),
                new FlatButton(
                  child: new Text(app.tr('camera')),
                  onPressed: () {
                    Navigator.of(context).pop();
                    getImageCamera();
                  },
                ),
              ],
            );
          },
        );
      },
      child: Text(app.tr('select')),
    );

    return Scaffold(
        key: _scaffoldKey,
        appBar: AppBar(
          title: Text(app.tr('elfarag')),
        ),
        body: GestureDetector(
          onTap: () {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          child: ListView(children: <Widget>[
            Form(
              key: _formKey,
              child: Container(
                color: Colors.white,
                child: Padding(
                  padding: const EdgeInsets.all(20.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      SizedBox(
                        height: 80.0,
                        width: 80.0,
                        child: CircleAvatar(
                          backgroundImage: _image != null
                              ? FileImage(_image)
                              : _photoUrl.isNotEmpty
                                  ? NetworkImage(_photoUrl)
                                  : AssetImage("images/user.png"),
                        ),
                      ),
                      selectImage,
                      SizedBox(height: 45.0),
                      emailField,
                      SizedBox(height: 15.0),
                      nameField,
                      SizedBox(
                        height: 35.0,
                      ),
                      loading ? CircularProgressIndicator() : loginButon,
                      SizedBox(
                        height: 15.0,
                      ),
                    ],
                  ),
                ),
              ),
            )
          ]),
        ));
  }
}
