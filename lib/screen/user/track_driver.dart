import 'package:dio/dio.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:elfarag/utils/Constant.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

class TrackDriver extends StatefulWidget {
  var currentUserKey = "";

  TrackDriver({this.currentUserKey});
  @override
  _TrackDriverState createState() => _TrackDriverState();
}

class _TrackDriverState extends State<TrackDriver> {
  String name, phone, carNumber, carModel = "";
  CameraPosition _kGooglePlex;

  @override
  void initState() {
    super.initState();
    fetchData();
  }

  void fetchData() async {
    Response response = await Dio().post("${Constant.BASE_URL}select_user.php",
        queryParameters: {"firebase_key": widget.currentUserKey, "phone": ""});
    if (response.statusCode == 200 && response.data['status']) {
      var user = response.data['data'];
      name = user[Constant.NAME_KEY];
      phone = user[Constant.PHONE_KEY];
      carNumber = user[Constant.CAR_NUMBER_KEY];
      carModel = user[Constant.CAR_MODEL_KEY];

       FirebaseDatabase.instance
        .reference()
        .child(Constant.USERS_KEY)
        .child(widget.currentUserKey)
        .onValue
        .listen((onValue) {
      setState(() {
        var user = onValue.snapshot.value;
        _kGooglePlex = CameraPosition(
          target: LatLng(user[Constant.LAT], user[Constant.LNG]),
          zoom: 14.4746,
        );
      });
    });
    }else{
      Navigator.of(context).pop();
    }
  }

  @override
  Widget build(BuildContext context) {
    var app = AppLocalizations.of(context);
    return new Scaffold(
        appBar: AppBar(),
        body: _kGooglePlex == null
            ? Center(child: Text(app.tr('please_wait')))
            : Stack(
                children: <Widget>[
                  GoogleMap(
                    myLocationButtonEnabled: false,
                    mapType: MapType.normal,
                    initialCameraPosition: _kGooglePlex,
                    onMapCreated: (GoogleMapController controller) {},
                    myLocationEnabled: false,
                    markers: Set<Marker>.of(<Marker>[
                      Marker(
                          markerId: MarkerId("1"),
                          position: _kGooglePlex.target)
                    ]),
                  ),
                  Align(
                    alignment: Alignment.bottomCenter,
                    child: Container(
                        margin: EdgeInsets.all(20),
                        padding: EdgeInsets.fromLTRB(12, 12, 0, 0),
                        decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius:
                              BorderRadius.all(const Radius.circular(5.0)),
                        ),
                        child: ListTile(
                          title: Text(name),
                          trailing: Text(phone),
                          subtitle: Column(
                            mainAxisSize: MainAxisSize.min,
                            crossAxisAlignment: CrossAxisAlignment.stretch,
                            children: <Widget>[
                              Padding(padding: EdgeInsets.only(top: 4)),
                              Text("${app.tr('car_number')} : $carNumber"),
                              Padding(padding: EdgeInsets.only(top: 4)),
                              Text("${app.tr('car_model')} : $carModel"),
                              Padding(
                                padding: EdgeInsets.only(bottom: 10),
                              )
                            ],
                          ),
                        )),
                  )
                ],
              ));
  }
}
