import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:elfarag/utils/Constant.dart';
import 'package:elfarag/utils/LocationOperation.dart';
import 'package:elfarag/utils/MyColor.dart';
import 'package:elfarag/utils/trip_status.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/material.dart';

import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:toast/toast.dart';
import 'package:http/http.dart' as http;

class CreateTrip extends StatefulWidget {
  @override
  _CreateTripState createState() => _CreateTripState();
}

enum TripAddress { FROM, TO }

class _CreateTripState extends State<CreateTrip> {
  var app;
  bool loading = false;
  bool haveTripDetails = false;
  String addressFrom = "";
  LatLng latLngFrom;
  String addressTo = "";
  LatLng latLngTo;
  String tripType = "";
  int tripTypeId = 0;
  String addressFromGMap = "";
  String addressToGMap = "";
  String totalPrice = "";
  String servicePrice = "";
  String timePrice = "";
  String kiloPrice = " ";
  String feesPrice = "";
  String note = "-";
  double totalPriceVal = 0;

  void getLocation(LatLng value, TripAddress type) async {
    setState(() {
      loading = true;
    });
    var address = "";
    List<Placemark> placemark = await Geolocator()
        .placemarkFromCoordinates(value.latitude, value.longitude);
    if (placemark[0] != null) {
      address = "${placemark[0].name} , ${placemark[0].subLocality} ";
    }

    setState(() {
      if (type == TripAddress.FROM) {
        addressFrom = address;
        latLngFrom = value;
      } else {
        addressTo = address;
        latLngTo = value;
      }
      loading = false;
    });
  }

  void _modalBottomSheetMenu() {
    showModalBottomSheet(
        context: context,
        builder: (builder) {
          return new ListView.builder(
            itemCount: Constant.servicesForUserString.length,
            itemBuilder: (BuildContext context, int index) {
              return ListTile(
                title: Text(app.tr(Constant.servicesForUserString[index + 2])),
                onTap: () {
                  Navigator.of(context).pop();
                  setState(() {
                    tripTypeId = index + 2;
                    tripType =
                        app.tr(Constant.servicesForUserString[index + 2]);
                  });
                },
              );
            },
          );
        });
  }

  void sendTripToManager() async {
    setState(() {
      loading = true;
    });
    final FirebaseUser currentUser = await FirebaseAuth.instance.currentUser();

    Dio dio = new Dio();
    var response = await dio.post(
        "https://alfaraj.sa/elfarag/create_trip_for_manager.php",
        queryParameters: {
          'from_lat': latLngFrom.latitude,
          'from_lng': latLngFrom.longitude,
          'from_text': addressFrom,
          'to_lat': latLngTo.latitude,
          'to_lng': latLngTo.longitude,
          'to_text': addressTo,
          'user_firebase_key':currentUser.uid,
          'note':note
        });
    if (response.statusCode == 200) {
      if (response.data['status']) {
        Navigator.of(context).pop();
        Toast.show(app.tr('your_request_sent'), context);
      } else {
        setState(() {
          loading = false;
          Toast.show(app.tr('error'), context);
        });
      }
    } else {
      setState(() {
        loading = false;
        Toast.show(app.tr('error'), context);
      });
    }
  }

  void _submit() {
    if (latLngTo == null || latLngFrom == null || tripTypeId == 0) {
      Toast.show(app.tr('invalid_input'), context);
    } else {
      if (tripTypeId > 10) {
        sendTripToManager();
      } else {
        calculateTripDetails();
      }
    }
  }

  // duplicated
  // void calculateTripDetails() async {

  //   setState(() {
  //     loading = true;
  //   });

  //   await FirebaseDatabase.instance
  //       .reference()
  //       .child("Service_Cost")
  //       .child("$tripTypeId")
  //       .once()
  //       .then((onValue) async {
  //     print(tripTypeId);
  //     var res = onValue.value;
  //     double price_per_min = double.parse(res['price_per_min']);
  //     double price_per_kilo = double.parse(res['price_per_kilo']);
  //     double price_per_service = double.parse(res['price_per_service']);

  //     if (tripTypeId <= 3) {
  //       // calculate from start to end

  //       double distanceInMeters = await Geolocator().distanceBetween(
  //           latLngFrom.latitude,
  //           latLngFrom.longitude,
  //           latLngTo.latitude,
  //           latLngTo.longitude);
  //       double totalDistance = distanceInMeters / 1000;
  //       double time = (totalDistance * 60) / 100;

  //       double total = (price_per_kilo * totalDistance) +
  //           (price_per_min * time) +
  //           price_per_service;

  //       totalPrice = "Approximate Price : ${total.round()} SAR ";

  //       servicePrice = "Service cost : $price_per_service SAR";
  //       timePrice = "Time cost : ${(price_per_min * time).round()} SAR";
  //       kiloPrice =
  //           "Distance cost : ${(price_per_kilo * totalDistance).round()} SAR";
  //     } else {
  //       // calculate from driver to client
  //       // ana ha5ly total ykon service price bs
  //       // 3shan m3rsh driver fen w hya5od time ad eh
  //       // bs h3mlo t2reby
  //       double totalFrom =
  //           (price_per_kilo * 10) + (price_per_min * 6) + price_per_service;
  //       double totalTo =
  //           (price_per_kilo * 100) + (price_per_min * 60) + price_per_service;
  //       servicePrice = "Service cost : $price_per_service SAR";
  //       timePrice =
  //           "Time cost : ${(price_per_min * 6).round()} - ${(price_per_min * 60).round()} SAR";
  //       totalPrice = "Price : ${totalFrom.round()} - ${totalTo.round()}";
  //       kiloPrice =
  //           "Distance cost : ${(price_per_kilo * 10).round()} - ${(price_per_kilo * 100).round()} SAR";
  //     }
  //     addressFromGMap = "From : " + addressFrom;
  //     addressToGMap = "To : " + addressTo;

  //     setState(() {
  //       loading = false;
  //       haveTripDetails = true;
  //     });
  //   });
  // }

  void calculateTripDetails() async {
    setState(() {
      loading = true;
    });

    await FirebaseDatabase.instance
        .reference()
        .child("Service_Cost")
        .child("$tripTypeId")
        .once()
        .then((onValue) async {
      var res = onValue.value;
      double price_per_min = double.parse(res['price_per_min']);
      double price_per_kilo = double.parse(res['price_per_kilo']);
      double price_per_service = double.parse(res['price_per_service']);
      double price_per_fees = double.parse(res['price_per_fees']);

      // get distance and time
      var response = await Dio().get(
          "https://maps.googleapis.com/maps/api/distancematrix/json?origins=${latLngFrom.latitude},${latLngFrom.longitude}&destinations=${latLngTo.latitude},${latLngTo.longitude}&key=${Constant.kGoogleApiKey}");

      var distance =
          response.data["rows"][0]['elements'][0]['distance']['value'];
      var duration =
          response.data["rows"][0]['elements'][0]['duration']['value'];

      double totalDistance = distance / 1000; // to convert to kilimiter
      double totalDuration = duration / 60; // to convert to mins

      totalPriceVal = (price_per_kilo * totalDistance) +
          (price_per_min * totalDuration) +
          price_per_service +
          price_per_fees;

      totalPrice =
          "${app.tr('approximate_cost')} : ${totalPriceVal.round()} SAR ";

      servicePrice = "${app.tr('service_cost')} : $price_per_service SAR";
      timePrice =
          "${app.tr('time_cost')} : ${(price_per_min * totalDuration).round()} SAR";
      kiloPrice =
          "${app.tr('distance_cost')} : ${(price_per_kilo * totalDistance).round()} SAR";

          feesPrice = "${app.tr('additional_price')} : ${price_per_fees.round()} SAR";

      addressFromGMap = "${app.tr('from')} : " + addressFrom;
      addressToGMap = "${app.tr('to')} : " + addressTo;

      setState(() {
        loading = false;
        haveTripDetails = true;
      });
    });
  }

  void cancelTrip() {
    addressFrom = "${app.tr('please_select')} ${app.tr('start_point')}";
    latLngFrom = null;
    addressTo = "${app.tr('please_select')} ${app.tr('end_point')}";
    latLngTo = null;
    tripType = "${app.tr('please_select')} ${app.tr('trip_type')}";
    tripTypeId = 0;
    setState(() {
      loading = false;
      haveTripDetails = false;
    });
  }

  void acceptTrip() async {
    setState(() {
      loading = true;
      haveTripDetails = false;
    });
    String driverKey = "";
    // Geofire.queryAtLocation(position.latitude, position.latitude, 10).listen((map) {
    //   print(map);
    // });

    FirebaseDatabase.instance
        .reference()
        .child(Constant.USERS_KEY)
        .orderByChild(Constant.IS_ONLINE_KEY)
        .startAt(true)
        .once()
        .then((DataSnapshot onValue) async {
      Map<dynamic, dynamic> map = onValue.value;
      if (map.length == 0) {
        Toast.show(app.tr('no_driver_available'), context);
        setState(() {
          loading = false;
          haveTripDetails = true;
        });
        return;
      }
      for (var item in map.entries) {
        // check if driver supoort trip type id
        // check if driver don't have trip
        List list = item.value[Constant.SERVICES_KEY].toString().split(',');
        if (!list.contains(tripTypeId.toString()) ||
            item.value[Constant.CURRENT_TRIP_KEY] != null) {
          continue;
        }

        // get location distance
        double distanceInMeters = LocationOperation.distance(
            latLngFrom.latitude,
            latLngFrom.longitude,
            item.value[Constant.LAT],
            item.value[Constant.LNG],
            LocationDistanceType.kilometers);
        if (distanceInMeters.toInt() <= 100) {
          driverKey = item.key;
          break;
        }
      }

      if (driverKey.isEmpty) {
        Toast.show(app.tr('no_driver_available'), context);
        setState(() {
          loading = false;
          haveTripDetails = true;
        });
      } else {
        createTripToMeAndToDriver(driverKey);
      }
    }).catchError((onError) {
      Toast.show(onError.toString(), context);
      setState(() {
        loading = false;
        haveTripDetails = true;
      });
    });
  }

  void createTripToMeAndToDriver(String driverKey) async {
    final FirebaseUser currentUser = await FirebaseAuth.instance.currentUser();

    String key = FirebaseDatabase.instance
        .reference()
        .child(Constant.TRIPS_KEY)
        .push()
        .key;

    FirebaseDatabase.instance
        .reference()
        .child(Constant.TRIPS_KEY)
        .child(key)
        .set({
      Constant.LAT_FROM_KEY: latLngFrom.latitude,
      Constant.LNG_FROM_KEY: latLngFrom.longitude,
      Constant.LAT_TO_KEY: latLngTo.latitude,
      Constant.LNG_TO_KEY: latLngTo.longitude,
      Constant.ADDRESS_FROM_KEY: addressFrom,
      Constant.ADDRESS_TO_KEY: addressTo,
      Constant.USER_ID_KEY: currentUser.uid,
      Constant.DRIVER_ID_KEY: driverKey,
      Constant.TRIP_TYPE_KEY: tripTypeId,
      Constant.TRIP_STATUS: TripStatus.created,
      Constant.CREATED_AT: ServerValue.timestamp,
      Constant.NOTE: note,
      Constant.PRICE: totalPriceVal
    });

    FirebaseDatabase.instance
        .reference()
        .child(Constant.USERS_KEY)
        .child(driverKey)
        .child(Constant.CURRENT_TRIP_KEY)
        .set(key)
        .then((_) {
      FirebaseDatabase.instance
          .reference()
          .child(Constant.USERS_KEY)
          .child(currentUser.uid)
          .child(Constant.CURRENT_TRIP_KEY)
          .set(key)
          .then((_) {
        Navigator.of(context).pop();
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    app = AppLocalizations.of(context);
    return Scaffold(
        appBar: AppBar(
          title: Text(app.tr('create_trip')),
        ),
        body: Container(
          color: MyColor.red,
          padding: EdgeInsets.all(10),
          child: Center(
            child: SingleChildScrollView(
              child: Column(
                children: <Widget>[
                  Container(
                    color: Colors.white,
                    child: Column(
                      children: <Widget>[
                        TextField(
                          onTap: () {
                            Navigator.of(context)
                                .pushNamed("/map")
                                .then((onval) {
                              var value = onval as LatLng;
                              print(value);
                              if (value != null) {
                                getLocation(value, TripAddress.FROM);
                              }
                            });
                          },
                          decoration: InputDecoration(
                              hintText: addressFrom.isEmpty
                                  ? "${app.tr('please_select')} ${app.tr('start_point')}"
                                  : addressFrom,
                              prefixIcon: Icon(Icons.swap_horizontal_circle)),
                          readOnly: true,
                        ),
                        TextField(
                          onTap: () {
                            Navigator.of(context)
                                .pushNamed("/map")
                                .then((onval) {
                              var value = onval as LatLng;
                              print(value);
                              if (value != null) {
                                getLocation(value, TripAddress.TO);
                              }
                            });
                          },
                          decoration: InputDecoration(
                              hintText: addressTo.isEmpty
                                  ? "${app.tr('please_select')} ${app.tr('end_point')}"
                                  : addressTo,
                              prefixIcon: Icon(Icons.swap_horizontal_circle)),
                          readOnly: true,
                        ),
                      ],
                    ),
                  ),
                  Container(
                    color: Colors.white,
                    margin: EdgeInsets.only(top: 20),
                    child: TextField(
                      onTap: () {
                        _modalBottomSheetMenu();
                      },
                      decoration: InputDecoration(
                          hintText: tripType.isEmpty
                              ? "${app.tr('please_select')} ${app.tr('trip_type')}"
                              : tripType,
                          prefixIcon: Icon(Icons.expand_more)),
                      readOnly: true,
                    ),
                  ),
                  Container(
                    color: Colors.white,
                    margin: EdgeInsets.only(top: 10),
                    child: TextField(
                      minLines: 2,
                      maxLines: 5,
                      onSubmitted: (String val) {
                        note = val;
                      },
                      keyboardType: TextInputType.multiline,
                      decoration: InputDecoration(
                          hintText: app.tr('note'),
                          prefixIcon: Icon(Icons.note)),
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.only(top: 10),
                  ),
                  loading
                      ? CircularProgressIndicator()
                      : haveTripDetails
                          ? Container()
                          : FlatButton(
                              child: Text(
                                app.tr('send'),
                                style: TextStyle(
                                    color: Colors.white, fontSize: 16),
                              ),
                              onPressed: () {
                                _submit();
                              },
                            ),
                  haveTripDetails
                      ? Container(
                          decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius:
                                BorderRadius.all(const Radius.circular(5.0)),
                          ),
                          padding: EdgeInsets.all(20),
                          child: Column(
                            mainAxisSize: MainAxisSize.max,
                            crossAxisAlignment: CrossAxisAlignment.stretch,
                            children: <Widget>[
                              Text(
                                app.tr('trip_details'),
                                textAlign: TextAlign.center,
                                style: TextStyle(fontWeight: FontWeight.bold),
                              ),
                              Padding(
                                padding: EdgeInsets.only(top: 8),
                              ),
                              Text(
                                addressFromGMap,
                                style: TextStyle(color: Colors.blue),
                              ),
                              Padding(
                                padding: EdgeInsets.only(top: 4),
                              ),
                              Text(
                                addressToGMap,
                                style: TextStyle(color: Colors.green),
                              ),
                              Padding(
                                padding: EdgeInsets.only(top: 4),
                              ),
                              Text(
                                servicePrice,
                              ),
                              Padding(
                                padding: EdgeInsets.only(top: 4),
                              ),
                              Text(
                                timePrice,
                              ),
                              Padding(
                                padding: EdgeInsets.only(top: 4),
                              ),
                              Text(
                                feesPrice,
                              ),
                              Padding(
                                padding: EdgeInsets.only(top: 4),
                              ),
                              Text(
                                kiloPrice,
                              ),
                              Divider(),
                              Padding(
                                padding: EdgeInsets.only(top: 4),
                              ),
                              Text(
                                totalPrice,
                              ),
                              Padding(
                                padding: EdgeInsets.only(top: 12),
                              ),
                              Row(
                                mainAxisSize: MainAxisSize.max,
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  FlatButton(
                                    child: Text(
                                      app.tr('reject'),
                                      style: TextStyle(
                                        color: Colors.red,
                                      ),
                                    ),
                                    onPressed: () {
                                      cancelTrip();
                                    },
                                  ),
                                  FlatButton(
                                    child: Text(
                                      app.tr('accept'),
                                      style: TextStyle(
                                        color: Colors.green,
                                      ),
                                    ),
                                    onPressed: () {
                                      acceptTrip();
                                    },
                                  ),
                                ],
                              )
                            ],
                          ),
                        )
                      : Container()
                ],
              ),
            ),
          ),
        ));
  }
}
