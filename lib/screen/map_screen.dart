import 'dart:async';

import 'package:easy_localization/easy_localization.dart';
import 'package:elfarag/utils/Constant.dart';
import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:toast/toast.dart';
import 'package:flutter_google_places/flutter_google_places.dart';
import 'package:google_maps_webservice/places.dart';

class MapScreen extends StatefulWidget {
  @override
  State<MapScreen> createState() => MapScreenState();
}

class MapScreenState extends State<MapScreen> {
  Completer<GoogleMapController> _controller = Completer();

  CameraPosition _kGooglePlex;
  LatLng myTarget;
  GoogleMapsPlaces _places;
  String searchTxtBtn = "Search  by name";

  @override
  void initState() {
    super.initState();
    _places = GoogleMapsPlaces(apiKey: Constant.kGoogleApiKey);
    getCurrentPosition();
  }

  void getCurrentPosition() async {
    var statue = await Geolocator().checkGeolocationPermissionStatus();
    Position position;
    if (statue != GeolocationStatus.granted) {
       position = await Geolocator().getLastKnownPosition();
    } else {
       position = await Geolocator()
          .getCurrentPosition(desiredAccuracy: LocationAccuracy.high);
      if (position.latitude == 0) {
        Toast.show("Please access location from setting", context);
        return;
      }
    }

    myTarget = new LatLng(position.latitude, position.longitude);
    setState(() {
      _kGooglePlex = CameraPosition(
        target: LatLng(position.latitude, position.longitude),
        zoom: 14.4746,
      );
    });
  }

  void showPlaceOption() async {
    Prediction p = await PlacesAutocomplete.show(
      context: context,
      apiKey: Constant.kGoogleApiKey,
      mode: Mode.overlay,
    );
    displayPrediction(p);
  }

  Future<Null> displayPrediction(Prediction p) async {
    if (p != null) {
      // get detail (lat/lng)
      PlacesDetailsResponse detail =
          await _places.getDetailsByPlaceId(p.placeId);
      final lat = detail.result.geometry.location.lat;
      final lng = detail.result.geometry.location.lng;
      myTarget = new LatLng(lat, lng);

      _kGooglePlex = CameraPosition(
        target: LatLng(lat, lng),
        zoom: 14.4746,
      );
      final GoogleMapController controller = await _controller.future;
      controller.animateCamera(CameraUpdate.newCameraPosition(_kGooglePlex));
      setState(() {
        myTarget = _kGooglePlex.target;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    var app = AppLocalizations.of(context);
    return new Scaffold(
      appBar: AppBar(),
      body: _kGooglePlex == null
          ? Center(
              child: CircularProgressIndicator(),
            )
          : Stack(
              children: <Widget>[
                GoogleMap(
                  mapType: MapType.normal,
                  initialCameraPosition: _kGooglePlex,
                  onTap: (LatLng pos) {
                    setState(() {
                      myTarget = pos;
                    });
                  },
                  onMapCreated: (GoogleMapController controller) {
                    _controller.complete(controller);
                  },
                  myLocationEnabled: true,
                  markers: Set<Marker>.of(<Marker>[
                    Marker(markerId: MarkerId("1"), position: myTarget)
                  ]),
                ),
                Container(
                  color: Colors.white,
                  height: 40,
                  margin: EdgeInsets.all(20),
                  child: TextField(
                    onTap: () {
                      showPlaceOption();
                    },
                    textAlign: TextAlign.center,
                    decoration: InputDecoration(hintText: app.tr("search")),
                  ),
                )
              ],
            ),
      floatingActionButton: FloatingActionButton.extended(
        onPressed: () {
          Navigator.pop(context, myTarget);
        },
        label: Text(app.tr("select")),
        icon: Icon(Icons.check),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
    );
  }
}
