import 'package:dio/dio.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:elfarag/screen/driver/pending_account.dart';
import 'package:elfarag/utils/Constant.dart';
import 'package:elfarag/utils/MyColor.dart';
import 'package:elfarag/utils/UserData.dart';
import 'package:elfarag/utils/Utils.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:toast/toast.dart';

final FirebaseAuth _auth = FirebaseAuth.instance;

class LoginScreen extends StatefulWidget {
  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  TextEditingController _phoneNumberController = new TextEditingController();
  TextEditingController _codeController = new TextEditingController();

  String _message = '';
  String _verificationId;

  bool secondStep = false;

  // Example code of how to verify phone number
  void _verifyPhoneNumber() async {
    Toast.show("Please Wait", context);

    final PhoneVerificationCompleted verificationCompleted =
        (AuthCredential phoneAuthCredential) {
      _auth.signInWithCredential(phoneAuthCredential);
    };

    final PhoneVerificationFailed verificationFailed =
        (AuthException authException) {
      _message =
          'Phone number verification failed. Code: ${authException.code}. Message: ${authException.message}';

      Toast.show(_message, context);
      print("gg $_message");
    };

    final PhoneCodeSent codeSent =
        (String verificationId, [int forceResendingToken]) async {
      Toast.show('Please check your phone for the verification code.', context);
      _verificationId = verificationId;
      setState(() {
        secondStep = true;
      });
    };

    final PhoneCodeAutoRetrievalTimeout codeAutoRetrievalTimeout =
        (String verificationId) {
      _verificationId = verificationId;
    };

    await _auth.verifyPhoneNumber(
        phoneNumber: _phoneNumberController.text,
        timeout: const Duration(seconds: 5),
        verificationCompleted: verificationCompleted,
        verificationFailed: verificationFailed,
        codeSent: codeSent,
        codeAutoRetrievalTimeout: codeAutoRetrievalTimeout);
  }

  // Example code of how to sign in with phone.
  void _signInWithPhoneNumber() async {
    final AuthCredential credential = PhoneAuthProvider.getCredential(
      verificationId: _verificationId,
      smsCode: _codeController.text,
    );
    final FirebaseUser user =
        (await _auth.signInWithCredential(credential).catchError((err) {
      Utils.showMyDialog(context, 'error', 'invalid_code');
    }))
            .user;
    final FirebaseUser currentUser = await _auth.currentUser();
    assert(user.uid == currentUser.uid);

    if (user != null) {
      Response response = await Dio().post(
          "${Constant.BASE_URL}select_user.php",
          queryParameters: {"phone": user.phoneNumber});
      
      if (response.statusCode == 200 && response.data['status']) {
        
        var user = response.data['data'];
        if (user == null) {
          Navigator.of(context).pushNamedAndRemoveUntil(
              '/user_type', (Route<dynamic> route) => false);
        } else if (user[Constant.USER_TYPE_KEY] == Constant.USER_KEY) {
          // todo : in future check if this account block or not
          Navigator.of(context).pushNamedAndRemoveUntil(
              '/user_home', (Route<dynamic> route) => false);
        } else if (user[Constant.USER_TYPE_KEY] == Constant.DRIVER_KEY) {
          if (user[Constant.ACCESS_KEY]) {
            UserData.tripTypes = user[Constant.SERVICES_KEY];
            Navigator.of(context).pushNamedAndRemoveUntil(
                '/driver_home', (Route<dynamic> route) => false);
          } else {
            Navigator.pushAndRemoveUntil(
                context,
                MaterialPageRoute(
                    builder: (context) => PendingAccount(
                          msg: Constant.PENDING_ACCOUNT,
                        )),
                (Route<dynamic> route) => false);
          }
        } else {
          FirebaseAuth.instance.signOut();
          Navigator.of(context).pushNamedAndRemoveUntil(
              '/login', (Route<dynamic> route) => false);
        }
      } else {
        Utils.showMyDialog(context, "error", 'contact_us');
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    var app = AppLocalizations.of(context);

    final phoneField = TextField(
      obscureText: false,
      controller: _phoneNumberController,
      keyboardType: TextInputType.phone,
      textAlignVertical: TextAlignVertical.center,
      decoration: InputDecoration(
        contentPadding: EdgeInsets.fromLTRB(10.0, 10.0, 10.0, 10.0),
        hintText: "+xxx xxxxxx",
      ),
    );

    final codeField = TextField(
      obscureText: false,
      controller: _codeController,
      keyboardType: TextInputType.number,
      maxLength: 6,
      textAlignVertical: TextAlignVertical.center,
      decoration: InputDecoration(
        contentPadding: EdgeInsets.fromLTRB(10.0, 10.0, 10.0, 10.0),
        hintText: app.tr('verification_code'),
      ),
    );

    final sendCode = Material(
        elevation: 5.0,
        borderRadius: BorderRadius.circular(30.0),
        color: MyColor.red,
        child: MaterialButton(
          minWidth: MediaQuery.of(context).size.width,
          onPressed: () {
            if (_phoneNumberController.text.trim().startsWith("+")) {
              _verifyPhoneNumber();
            } else {
              Toast.show(app.tr('enter_phone_code'), context);
            }
          },
          child: Text(
            app.tr('send'),
            textAlign: TextAlign.center,
            style: TextStyle(color: Colors.white),
          ),
        ));

    final verifyCode = Material(
        elevation: 5.0,
        borderRadius: BorderRadius.circular(30.0),
        color: MyColor.red,
        child: MaterialButton(
          minWidth: MediaQuery.of(context).size.width,
          onPressed: () {
            _signInWithPhoneNumber();
          },
          child: Text(
            app.tr('verify_code'),
            textAlign: TextAlign.center,
            style: TextStyle(color: Colors.white),
          ),
        ));

    return Scaffold(
        backgroundColor: Colors.white,
        body: GestureDetector(
          onTap: () {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          child: ListView(
            children: <Widget>[
              Container(
                color: Colors.white,
                child: Padding(
                  padding: const EdgeInsets.all(36.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      SizedBox(
                        height: 200.0,
                        width: 230,
                        // child: Image.asset(
                        //   "assets/logo.png",
                        //   fit: BoxFit.contain,
                        // ),
                        child: CircleAvatar(
                            backgroundImage: AssetImage("images/logo.png"),
                            backgroundColor: Colors.white),
                      ),
                      SizedBox(height: 45.0),
                      Text(app.tr("please_login")),
                      SizedBox(
                        height: 15.0,
                      ),
                      phoneField,
                      SizedBox(
                        height: 15.0,
                      ),
                      SizedBox(
                        child: sendCode,
                        width: 150,
                        height: 50.0,
                      ),
                      SizedBox(
                        height: 25.0,
                      ),
                      secondStep ? codeField : Container(),
                      secondStep
                          ? SizedBox(
                              child: verifyCode,
                              width: 150,
                              height: 50.0,
                            )
                          : Container(),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ));
  }
}
