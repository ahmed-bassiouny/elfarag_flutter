import 'package:easy_localization/easy_localization.dart';
import 'package:elfarag/utils/Constant.dart';
import 'package:flutter/material.dart';

class SelectServiceType extends StatefulWidget {
  @override
  _SelectServiceTypeState createState() => _SelectServiceTypeState();
}

class _SelectServiceTypeState extends State<SelectServiceType> {
  @override
  Widget build(BuildContext context) {
    var app = AppLocalizations.of(context);
    return Scaffold(
      appBar: AppBar(),
      body: ListView(
          children: Constant.servicesForDriver.keys.map((int key) {
        return CheckboxListTile(
          title: Text(app.tr(Constant.servicesForDriverString[key])),
          value: Constant.servicesForDriver[key],
          activeColor: Theme.of(context).primaryColor,
          onChanged: (bool val) {
            setState(() {
              Constant.servicesForDriver[key] = val;
            });
          },
        );
      }).toList()),
    );
  }
}
