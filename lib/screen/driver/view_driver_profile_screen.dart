import 'dart:io';

import 'package:dio/dio.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:elfarag/main.dart';
import 'package:elfarag/screen/driver/pending_account.dart';
import 'package:elfarag/utils/Constant.dart';
import 'package:elfarag/utils/MyColor.dart';
import 'package:elfarag/utils/UserData.dart';
import 'package:elfarag/utils/Utils.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:toast/toast.dart';

class ViewDriverProfileScreen extends StatefulWidget {
  @override
  _ViewDriverProfileScreenState createState() =>
      _ViewDriverProfileScreenState();
}


class _ViewDriverProfileScreenState extends State<ViewDriverProfileScreen> {
  String _profileImageUrl = "";
  String _personalNumberUrl = "";
  String _carLicenseUrl = "";
  String _insuranceUrl = "";
  String _driverLicenseUrl = "";

  String _name,
      _email,
      _ibanNumber,
      _city,
      _personalNumber,
      _carType,
      _carNumber,
      _carModel;

  bool loading = true;

  @override
  initState() {
    super.initState();
    fetchData();
  }

  fetchData() async {
    FirebaseUser currentUser = await FirebaseAuth.instance.currentUser();
    Response response = await Dio().post("${Constant.BASE_URL}select_user.php",
        queryParameters: {"phone": currentUser.phoneNumber});
    if (response.statusCode == 200 && response.data['status']) {
      var user = response.data['data'];
      if (user != null) {
        setState(() {
          _name = user[Constant.NAME_KEY];
          _email = user[Constant.EMAIL_KEY];
          _ibanNumber = user[Constant.IBAN_NUMBER_KEY];
          _city = user[Constant.CITY_KEY];
          _personalNumber = user[Constant.PERSONAL_NUMBER_KEY];
          _carType = user[Constant.CAR_TYPE_KEY];
          _carNumber = user[Constant.CAR_NUMBER_KEY];
          _carModel = user[Constant.CAR_MODEL_KEY];

          _profileImageUrl = user[Constant.PHOTO_KEY];
          _personalNumberUrl = user[Constant.PERSONAL_NUMBER_URL_KEY];
          _carLicenseUrl = user[Constant.CAR_LICENSE_URL_KEY];
          _insuranceUrl = user[Constant.INSURANCE_URL_KEY];
          _driverLicenseUrl = user[Constant.DRIVER_LICENSE_URL_KEY];
          loading = false;
        });
      }else{
        Navigator.of(context).pop();
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    var app = AppLocalizations.of(context);
    final nameField = TextFormField(
      decoration: InputDecoration(labelText: app.tr('name')),
      enabled: false,
      initialValue: _name,
    );

    final emailField = TextFormField(
      enabled: false,
      initialValue:  _email,
      decoration: InputDecoration(labelText: app.tr('email')),
    );

    final ibanNumberField = TextFormField(
      
      enabled: false,
      initialValue: _ibanNumber,
      decoration: InputDecoration(labelText: app.tr('iban_number')),
    );

    final cityField = TextFormField(
      initialValue: _city,
      enabled: false,
      decoration: InputDecoration(labelText: app.tr('city')),
    );

    final personalNumberField = TextFormField(
      initialValue: _personalNumber,
      enabled: false,
      decoration: InputDecoration(labelText: app.tr('personal_number')),
    );

    final carTypeField = TextFormField(
      initialValue: _carType,
      enabled: false,
      decoration: InputDecoration(labelText: app.tr('car_type')),
    );

    final carNumberField = TextFormField(
      initialValue: _carNumber,
      enabled: false,
      decoration: InputDecoration(labelText: app.tr('car_number')),
    );

    final carModelField = TextFormField(
      initialValue: _carModel,
      enabled: false,
      decoration: InputDecoration(labelText: app.tr('car_model')),
    );

   

    return Scaffold(
      appBar: AppBar(
        title: Text(app.tr('profile')),
      ),
      body: loading ? CircularProgressIndicator() :SingleChildScrollView(
        padding: EdgeInsets.all(20),
        child: Column(
          children: <Widget>[
            Container(
              child: Text(
                app.tr('personal_info'),
                style: TextStyle(fontSize: 16, color: MyColor.green),
                textAlign: TextAlign.center,
              ),
              padding: EdgeInsets.only(top: 20),
            ),
            Padding(
              padding: EdgeInsets.only(top: 20),
            ),
            Image.network(
              _profileImageUrl,
              width: 100,
              height: 100,
            ),
            nameField,
            emailField,
            ibanNumberField,
            cityField,
            personalNumberField,
            Padding(
              padding: EdgeInsets.only(top: 20),
            ),
            Text(app.tr('personal_number')),
            Padding(
              padding: EdgeInsets.only(top: 10),
            ),
            Image.network(
              _personalNumberUrl,
              width: 300,
              height: 150,
            ),
            Container(
              child: Text(
                app.tr('car_info'),
                style: TextStyle(fontSize: 16, color: MyColor.green),
                textAlign: TextAlign.center,
              ),
              padding: EdgeInsets.only(top: 20),
            ),
            Padding(
              padding: EdgeInsets.only(top: 10),
            ),
            Text(app.tr('car_license')),
            Padding(
              padding: EdgeInsets.only(top: 10),
            ),
            Image.network(
              _carLicenseUrl,
              width: 300,
              height: 150,
            ),
            carTypeField,
            carNumberField,
            carModelField,
            Padding(
              padding: EdgeInsets.only(top: 20),
            ),
            Text(app.tr('insurance_image')),
            Padding(
              padding: EdgeInsets.only(top: 10),
            ),
            Image.network(
              _insuranceUrl,
              width: 300,
              height: 150,
            ),
            Padding(
              padding: EdgeInsets.only(top: 10),
            ),
            Text(app.tr('driver_license')),
            Padding(
              padding: EdgeInsets.only(top: 10),
            ),
            Image.network(
              _driverLicenseUrl,
              width: 300,
              height: 150,
            ),
            Padding(
              padding: EdgeInsets.only(top: 10),
            ),
            Padding(
              padding: EdgeInsets.only(top: 10),
            )
          ],
        ),
      ),
    );
  }
}
