import 'package:easy_localization/easy_localization.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';

class PendingAccount extends StatelessWidget {
  final String msg;

  PendingAccount({this.msg});

  @override
  Widget build(BuildContext context) {
    var app = AppLocalizations.of(context);
    return Scaffold(
      appBar: AppBar(
        title: Text(app.tr('elfarag')),
        actions: <Widget>[
          FlatButton(
            child: Text(
              app.tr('logout'),
              style: TextStyle(color: Colors.white),
            ),
            onPressed: () {
              FirebaseAuth.instance.signOut();
              Navigator.of(context).pushNamedAndRemoveUntil(
                  '/splash', (Route<dynamic> route) => false);
            },
          )
        ],
      ),
      body: Center(
          child: Text(
        msg,
        style: TextStyle(fontSize: 16),
        textAlign: TextAlign.center,
      )),
    );
  }
}
