import 'dart:io';

import 'package:dio/dio.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:elfarag/screen/driver/pending_account.dart';
import 'package:elfarag/utils/Constant.dart';
import 'package:elfarag/utils/MyColor.dart';
import 'package:elfarag/utils/UserData.dart';
import 'package:elfarag/utils/Utils.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:toast/toast.dart';

import '../../utils/Constant.dart';
import '../../utils/Constant.dart';

class UpdateDriverProfileScreen extends StatefulWidget {
  @override
  _UpdateDriverProfileScreenState createState() =>
      _UpdateDriverProfileScreenState();
}

enum FileType {
  PROFILE_IMAGE,
  PERSONAL_NUMBER,
  CAR_LICENSE,
  DRIVER_LICENSE,
  INSURANCE
}

class _UpdateDriverProfileScreenState extends State<UpdateDriverProfileScreen> {
  File _profileImageFile,
      _personalNumberFile,
      _carLicenseFile,
      _insuranceFile,
      _driverLicenseFile;

  String _name,
      _email,
      _ibanNumber,
      _city,
      _personalNumber,
      _carType,
      _carNumber,
      _carModel;

  bool loading = false;

  final _formKey = GlobalKey<FormState>();
  FirebaseUser currentUser;

  @override
  initState() {
    super.initState();
    fetchData();
  }

  fetchData() async {
    currentUser = await FirebaseAuth.instance.currentUser();
  }

  Future getImage(FileType type, ImageSource source) async {
    var image = await ImagePicker.pickImage(source: source);
    setState(() {
      switch (type) {
        case FileType.PROFILE_IMAGE:
          _profileImageFile = image;
          break;
        case FileType.PERSONAL_NUMBER:
          _personalNumberFile = image;
          break;
        case FileType.CAR_LICENSE:
          _carLicenseFile = image;
          break;
        case FileType.DRIVER_LICENSE:
          _driverLicenseFile = image;
          break;
        case FileType.INSURANCE:
          _insuranceFile = image;
          break;
      }
    });
  }

  Future uploadPicAndSaveUser(String services) async {
    if (currentUser == null) return;
    String url = 'https://alfaraj.sa/elfarag/upload_multi.php';
    if (_profileImageFile == null) {
      Toast.show("Complete Images", context);
      return;
    } else if (_personalNumberFile == null) {
      Toast.show("Complete Images", context);
      return;
    } else if (_carLicenseFile == null) {
      Toast.show("Complete Images", context);
      return;
    } else if (_insuranceFile == null) {
      Toast.show("Complete Images", context);
      return;
    } else if (_driverLicenseFile == null) {
      Toast.show("Complete Images", context);
      return;
    }
    setState(() {
      loading = true;
    });

    File compressedProfileImageFile = await Utils.compress(_profileImageFile);
    File compressedPersonalNumberFile =
        await Utils.compress(_personalNumberFile);
    File compressedCarLicenseFile = await Utils.compress(_carLicenseFile);
    File compressedInsuranceFile = await Utils.compress(_insuranceFile);
    File compressedDriverLicenseFile = await Utils.compress(_driverLicenseFile);

    FormData formData = FormData.fromMap({
      "name": currentUser.uid,
      "avatar": _profileImageFile == null
          ? null
          : await MultipartFile.fromFile(compressedProfileImageFile.path,
              filename: _profileImageFile.path.split("/").last),
      "personal_number": _personalNumberFile == null
          ? null
          : await MultipartFile.fromFile(compressedPersonalNumberFile.path,
              filename: _personalNumberFile.path.split("/").last),
      "car_license": _carLicenseFile == null
          ? null
          : await MultipartFile.fromFile(compressedCarLicenseFile.path,
              filename: _carLicenseFile.path.split("/").last),
      "insurance": _insuranceFile == null
          ? null
          : await MultipartFile.fromFile(compressedInsuranceFile.path,
              filename: _insuranceFile.path.split("/").last),
      "driver_license": _driverLicenseFile == null
          ? null
          : await MultipartFile.fromFile(compressedDriverLicenseFile.path,
              filename: _driverLicenseFile.path.split("/").last),
    });

    var response = await Dio().post(url, data: formData);
    String profileImageUrl = "";
    String driverLicenseUrl = "";
    String personalNumberUrl = "";
    String carLicenseUrl = "";
    String insuranceUrl = "";

    if (response.data['status']) {
      print(response.data);
      if (response.data['avatar'].toString().isNotEmpty) {
        profileImageUrl = response.data['avatar'];
      }
      if (response.data['personal_number'].toString().isNotEmpty) {
        personalNumberUrl = response.data['personal_number'];
      }
      if (response.data['car_license'].toString().isNotEmpty) {
        carLicenseUrl = response.data['car_license'];
      }
      if (response.data['insurance'].toString().isNotEmpty) {
        insuranceUrl = response.data['insurance'];
      }
      if (response.data['driver_license'].toString().isNotEmpty) {
        driverLicenseUrl = response.data['driver_license'];
      }
      saveUserInfo(services,
          carLicenseUrl: carLicenseUrl,
          profileImageUrl: profileImageUrl,
          insuranceUrl: insuranceUrl,
          personalNumberUrl: personalNumberUrl,
          driverLicenseUrl: driverLicenseUrl);
    } else {
      setState(() {
        loading = false;
        Toast.show("Error", context);
      });
    }
  }

  void saveUserInfo(String services,
      {String profileImageUrl,
      String insuranceUrl = "",
      String personalNumberUrl = "",
      String driverLicenseUrl = "",
      String carLicenseUrl = ""}) async {
    Response response = await Dio()
        .post("${Constant.BASE_URL}create_driver.php", queryParameters: {
      Constant.EMAIL_KEY: _email,
      Constant.NAME_KEY: _name,
      Constant.PHONE_KEY: currentUser.phoneNumber,
      Constant.CAR_TYPE_KEY: _carType,
      Constant.CITY_KEY: _city,
      Constant.CAR_NUMBER_KEY: _carNumber,
      Constant.CAR_MODEL_KEY: _carModel,
      Constant.PERSONAL_NUMBER_KEY: _personalNumber,
      Constant.IBAN_NUMBER_KEY: _ibanNumber,
      Constant.SERVICES_KEY: services,
      Constant.PHOTO_KEY: profileImageUrl,
      Constant.INSURANCE_URL_KEY: insuranceUrl,
      Constant.PERSONAL_NUMBER_URL_KEY: personalNumberUrl,
      Constant.DRIVER_LICENSE_URL_KEY: driverLicenseUrl,
      Constant.CAR_LICENSE_URL_KEY: carLicenseUrl,
      "firebase_key": currentUser.uid
    });
    print(response);
    if (response.statusCode == 200 && response.data['status']) {
      var user = response.data['data'];
      UserData.tripTypes = services;
      // save user in firebase base
      await FirebaseDatabase.instance
          .reference()
          .child(Constant.USERS_KEY)
          .child(currentUser.uid)
          .set({
        Constant.IS_ONLINE_KEY: false,
        Constant.SERVICES_KEY: services,
      }).then((_) {
        if (user[Constant.BLOCK_KEY]) {
          Navigator.pushAndRemoveUntil(
              context,
              MaterialPageRoute(
                  builder: (context) => PendingAccount(
                        msg: Constant.BLOCK_ACCOUNT,
                      )),
              (Route<dynamic> route) => false);
        } else if (user[Constant.ACCESS_KEY]) {
          Navigator.of(context).pushNamedAndRemoveUntil(
              '/driver_home', (Route<dynamic> route) => false);
        } else {
          Navigator.pushAndRemoveUntil(
              context,
              MaterialPageRoute(
                  builder: (context) => PendingAccount(
                        msg: Constant.PENDING_ACCOUNT,
                      )),
              (Route<dynamic> route) => false);
        }
      }).catchError((onError) {
        setState(() {
          loading = false;
        });
        Toast.show(onError.toString(), context);
      });
    } else {
      setState(() {
        loading = false;
      });
      Utils.showMyDialog(context, "error", 'invalid_input');
    }
  }

  @override
  Widget build(BuildContext context) {
    var app = AppLocalizations.of(context);

    final nameField = TextFormField(
      decoration: InputDecoration(labelText: app.tr('name')),
      onSaved: (String val) => _name = val,
      validator: (String value) {
        return value.trim().length < 5 ? app.tr('invalid_input') : null;
      },
    );

    final emailField = TextFormField(
      keyboardType: TextInputType.emailAddress,
      onSaved: (String val) => _email = val,
      validator: (String value) => (value.contains('@') && value.contains('.'))
          ? null
          : app.tr('invalid_input'),
      decoration: InputDecoration(labelText: app.tr('email')),
    );

    final ibanNumberField = TextFormField(
      onSaved: (String val) => _ibanNumber = val,
      keyboardType: TextInputType.text,
      validator: (String value) {
        return value.trim().isEmpty ? app.tr('invalid_input') : null;
      },
      decoration: InputDecoration(labelText: app.tr('iban_number')),
    );

    final cityField = TextFormField(
      onSaved: (String val) => _city = val,
      validator: (String value) {
        return value.trim().isEmpty ? app.tr('invalid_input') : null;
      },
      decoration: InputDecoration(labelText: app.tr('city')),
    );

    final personalNumberField = TextFormField(
      onSaved: (String val) => _personalNumber = val,
      keyboardType: TextInputType.text,
      validator: (String value) {
        return value.trim().isEmpty ? app.tr('invalid_input') : null;
      },
      decoration: InputDecoration(labelText: app.tr('personal_number')),
    );

    final carTypeField = TextFormField(
      onSaved: (String val) => _carType = val,
      keyboardType: TextInputType.text,
      validator: (String value) {
        return value.trim().isEmpty ? app.tr('invalid_input') : null;
      },
      decoration: InputDecoration(labelText: app.tr('car_type')),
    );

    final carNumberField = TextFormField(
      onSaved: (String val) => _carNumber = val,
      keyboardType: TextInputType.text,
      validator: (String value) {
        return value.trim().isEmpty ? app.tr('invalid_input') : null;
      },
      decoration: InputDecoration(labelText: app.tr('car_number')),
    );

    final carModelField = TextFormField(
      onSaved: (String val) => _carModel = val,
      keyboardType: TextInputType.text,
      validator: (String value) {
        return value.trim().isEmpty ? app.tr('invalid_input') : null;
      },
      decoration: InputDecoration(labelText: app.tr('car_model')),
    );

    String getServicesId() {
      String trips = "";
      int lenght = Constant.servicesForDriver.length;
      for (int i = 2; i <= lenght; i++) {
        if (Constant.servicesForDriver[i]) {
          trips += "$i,";
        }
      }
      if (trips.length > 0) {
        trips = trips.substring(0, trips.length - 1);
      }
      return trips;
    }

    void _submit() {
      if (_formKey.currentState.validate()) {
        _formKey.currentState.save();
        String services = getServicesId();
        if (services.isEmpty) {
          Toast.show(app.tr('select_service'), context);
        } else {
          uploadPicAndSaveUser(services);
        }
      }
    }

    final saveButon = FlatButton(
      onPressed: () {
        _submit();
      },
      child: Text(
        app.tr('save'),
        textAlign: TextAlign.center,
        style: TextStyle(color: Theme.of(context).primaryColor),
      ),
    );

    void showDialogForImage(FileType type) {
      showDialog(
        context: context,
        builder: (BuildContext context) {
          // return object of type Dialog
          return AlertDialog(
            title: new Text(app.tr('change_image')),
            content: new Text(app.tr('select_image')),
            actions: <Widget>[
              // usually buttons at the bottom of the dialog
              new FlatButton(
                child: new Text(app.tr('gallery')),
                onPressed: () {
                  Navigator.of(context).pop();
                  getImage(type, ImageSource.gallery);
                },
              ),
              new FlatButton(
                child: new Text(app.tr('camera')),
                onPressed: () {
                  Navigator.of(context).pop();
                  getImage(type, ImageSource.camera);
                },
              ),
            ],
          );
        },
      );
    }

    return Scaffold(
      appBar: AppBar(
        title: Text("El Farag"),
      ),
      body: Form(
        key: _formKey,
        child: SingleChildScrollView(
          padding: EdgeInsets.all(20),
          child: Column(
            children: <Widget>[
              Container(
                child: Text(
                  app.tr('personal_info'),
                  style: TextStyle(fontSize: 16, color: MyColor.green),
                  textAlign: TextAlign.center,
                ),
                padding: EdgeInsets.only(top: 20),
              ),
              Padding(
                padding: EdgeInsets.only(top: 20),
              ),
              GestureDetector(
                child: _profileImageFile != null
                    ? Image.file(
                        _profileImageFile,
                        width: 100,
                        height: 100,
                      )
                    : Image.asset(
                        "images/user.png",
                        width: 80,
                        height: 80,
                      ),
                onTap: () {
                  showDialogForImage(FileType.PROFILE_IMAGE);
                },
              ),
              nameField,
              emailField,
              ibanNumberField,
              cityField,
              personalNumberField,
              Padding(
                padding: EdgeInsets.only(top: 20),
              ),
              Text(app.tr('personal_number')),
              Padding(
                padding: EdgeInsets.only(top: 10),
              ),
              GestureDetector(
                child: _personalNumberFile != null
                    ? Image.file(_personalNumberFile)
                    : Image.asset(
                        "images/placeholder.jpg",
                        height: 150,
                        width: 200,
                        fit: BoxFit.cover,
                      ),
                onTap: () {
                  showDialogForImage(FileType.PERSONAL_NUMBER);
                },
              ),
              Container(
                child: Text(
                  app.tr('car_info'),
                  style: TextStyle(fontSize: 16, color: MyColor.green),
                  textAlign: TextAlign.center,
                ),
                padding: EdgeInsets.only(top: 20),
              ),
              Padding(
                padding: EdgeInsets.only(top: 10),
              ),
              Text(app.tr('car_license')),
              Padding(
                padding: EdgeInsets.only(top: 10),
              ),
              GestureDetector(
                child: _carLicenseFile != null
                    ? Image.file(_carLicenseFile)
                    : Image.asset(
                        "images/placeholder.jpg",
                        height: 150,
                        width: 200,
                        fit: BoxFit.cover,
                      ),
                onTap: () {
                  showDialogForImage(FileType.CAR_LICENSE);
                },
              ),
              carTypeField,
              carNumberField,
              carModelField,
              Padding(
                padding: EdgeInsets.only(top: 20),
              ),
              Text(app.tr('service_type')),
              Center(
                child: FlatButton(
                  child: Text(
                    app.tr('click_here'),
                    style: TextStyle(color: Theme.of(context).accentColor),
                  ),
                  onPressed: () {
                    Navigator.of(context).pushNamed("/service_type");
                  },
                ),
              ),
              Padding(
                padding: EdgeInsets.only(top: 20),
              ),
              Text(app.tr('insurace_image')),
              Padding(
                padding: EdgeInsets.only(top: 10),
              ),
              GestureDetector(
                child: _insuranceFile != null
                    ? Image.file(_insuranceFile)
                    : Image.asset(
                        "images/placeholder.jpg",
                        height: 150,
                        width: 200,
                        fit: BoxFit.cover,
                      ),
                onTap: () {
                  showDialogForImage(FileType.INSURANCE);
                },
              ),
              Padding(
                padding: EdgeInsets.only(top: 10),
              ),
              Text(app.tr('driver_license')),
              Padding(
                padding: EdgeInsets.only(top: 10),
              ),
              GestureDetector(
                child: _driverLicenseFile != null
                    ? Image.file(_driverLicenseFile)
                    : Image.asset(
                        "images/placeholder.jpg",
                        height: 150,
                        width: 200,
                        fit: BoxFit.cover,
                      ),
                onTap: () {
                  showDialogForImage(FileType.DRIVER_LICENSE);
                },
              ),
              Padding(
                padding: EdgeInsets.only(top: 10),
              ),
              loading
                  ? Row(
                      children: <Widget>[
                        CircularProgressIndicator(),
                        Text(app.tr('please_wait'))
                      ],
                    )
                  : saveButon,
              Padding(
                padding: EdgeInsets.only(top: 10),
              )
            ],
          ),
        ),
      ),
    );
  }
}
