import 'dart:async';

import 'package:dio/dio.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:elfarag/main.dart';
import 'package:elfarag/utils/Constant.dart';
import 'package:elfarag/utils/UserData.dart';
import 'package:elfarag/utils/trip_status.dart';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:share/share.dart';
import 'package:toast/toast.dart';
import 'package:url_launcher/url_launcher.dart';

class DriverHomeScreen extends StatefulWidget {
  @override
  _DriverHomeScreenState createState() => _DriverHomeScreenState();
}

class _DriverHomeScreenState extends State<DriverHomeScreen> {
  FirebaseUser currentUser;
  bool isOnline = false;
  ProgressDialog pr;
  CameraPosition _kGooglePlex;
  LatLng myTarget;
  LatLng markerLatLng;
  Timer timer;
  bool haveTrip = false;
  StreamSubscription<Event> tripValues;
  String address = "";
  String tripType = "";
  String note = "";
  String agreeBtn = "";
  String currentTripId = "";
  String userId = "";
  int tripStatus = 0;
  Map trip;
  var app;

  void initUser() async {
    currentUser = await FirebaseAuth.instance.currentUser();
    setUserOfflineWhenCloseApp();
    fetchData();
  }

  void fetchData() async {
    final FirebaseUser currentUser = await FirebaseAuth.instance.currentUser();

    FirebaseDatabase.instance
        .reference()
        .child(Constant.USERS_KEY)
        .child(currentUser.uid)
        .child(Constant.CURRENT_TRIP_KEY)
        .onValue
        .listen((onValue) {
      currentTripId = onValue.snapshot.value;

      setState(() {
        if (currentTripId == null || currentTripId.isEmpty) {
          haveTrip = false;
        } else {
          trackTrip();
        }
      });
    });
  }

  void trackTrip() {
    tripValues = FirebaseDatabase.instance
        .reference()
        .child(Constant.TRIPS_KEY)
        .child(currentTripId)
        .onValue
        .listen((onData) {
      trip = onData.snapshot.value;
      userId = trip[Constant.USER_ID_KEY];
      tripType =
          "${app.tr('trip_type')} :  ${app.tr(Constant.servicesForUserString[trip[Constant.TRIP_TYPE_KEY]])}";
      note = "${app.tr('note')} : ${trip[Constant.NOTE]}";
      tripStatus = trip[Constant.TRIP_STATUS];
      switch (tripStatus) {
        case 0:
          address = trip[Constant.ADDRESS_FROM_KEY];
          agreeBtn = app.tr('go_to_client');
          break;
        case 7:
          agreeBtn = app.tr('arrive');
          break;
        case 5:
          agreeBtn = app.tr('finish');
          break;
        case 3:
          agreeBtn = app.tr('finish');
          break;
        default:
          break;
      }

      setState(() {
        haveTrip = true;
      });
    });
  }

  void handleFirstType() async {
    // from and to
    Position position = await Geolocator()
        .getCurrentPosition(desiredAccuracy: LocationAccuracy.high);
    if (position.latitude == 0) return;

    if (tripStatus == TripStatus.created) {
      trip[Constant.TRIP_STATUS] = TripStatus.goToClient;
      FirebaseDatabase.instance
          .reference()
          .child(Constant.TRIPS_KEY)
          .child(currentTripId)
          .set(trip);

      setState(() {
        markerLatLng =
            LatLng(trip[Constant.LAT_FROM_KEY], trip[Constant.LNG_FROM_KEY]);
      });
    } else if (tripStatus == TripStatus.goToClient) {
      trip[Constant.TRIP_STATUS] = TripStatus.startTrip;
      trip["realLocationFrom"] = {
        Constant.LAT: position.latitude,
        Constant.LNG: position.longitude,
        Constant.TIME_KEY: ServerValue.timestamp,
      };
      FirebaseDatabase.instance
          .reference()
          .child(Constant.TRIPS_KEY)
          .child(currentTripId)
          .set(trip);
      setState(() {
        markerLatLng =
            LatLng(trip[Constant.LAT_TO_KEY], trip[Constant.LNG_TO_KEY]);
      });
    } else if (tripStatus == TripStatus.startTrip) {
      trip[Constant.TRIP_STATUS] = TripStatus.endTrip;
      trip["realLocationTo"] = {
        Constant.LAT: position.latitude,
        Constant.LNG: position.longitude,
        Constant.TIME_KEY: ServerValue.timestamp,
      };
      FirebaseDatabase.instance
          .reference()
          .child(Constant.TRIPS_KEY)
          .child(currentTripId)
          .set(trip);

      await FirebaseDatabase.instance
          .reference()
          .child(Constant.USERS_KEY)
          .child(userId)
          .child(Constant.CURRENT_TRIP_KEY)
          .remove();

      await FirebaseDatabase.instance
          .reference()
          .child(Constant.USERS_KEY)
          .child(currentUser.uid)
          .child(Constant.CURRENT_TRIP_KEY)
          .remove();
      setState(() {
        markerLatLng = null;
      });
    }
  }

  // void handleSecondType() async {
  //   // from and to
  //   Position position = await Geolocator()
  //       .getCurrentPosition(desiredAccuracy: LocationAccuracy.high);
  //   if (position.latitude == 0) return;

  //   if (tripStatus == TripStatus.created) {
  //     trip[Constant.TRIP_STATUS] = TripStatus.goToClientFromMyPoint;
  //     trip["realLocationFrom"] = {
  //       Constant.LAT: position.latitude,
  //       Constant.LNG: position.longitude,
  //       Constant.TIME_KEY: ServerValue.timestamp,
  //     };
  //     FirebaseDatabase.instance
  //         .reference()
  //         .child(Constant.TRIPS_KEY)
  //         .child(currentTripId)
  //         .set(trip);

  //     setState(() {
  //       markerLatLng =
  //           LatLng(trip[Constant.LAT_FROM_KEY], trip[Constant.LNG_FROM_KEY]);
  //     });
  //   } else if (tripStatus == TripStatus.goToClientFromMyPoint) {
  //     trip[Constant.TRIP_STATUS] = TripStatus.finish;
  //     trip["realLocationTo"] = {
  //       Constant.LAT: position.latitude,
  //       Constant.LNG: position.longitude,
  //       Constant.TIME_KEY: ServerValue.timestamp,
  //     };
  //     FirebaseDatabase.instance
  //         .reference()
  //         .child(Constant.TRIPS_KEY)
  //         .child(currentTripId)
  //         .set(trip);
  //     await FirebaseDatabase.instance
  //         .reference()
  //         .child(Constant.USERS_KEY)
  //         .child(userId)
  //         .child(Constant.CURRENT_TRIP_KEY)
  //         .remove();

  //     await FirebaseDatabase.instance
  //         .reference()
  //         .child(Constant.USERS_KEY)
  //         .child(currentUser.uid)
  //         .child(Constant.CURRENT_TRIP_KEY)
  //         .remove();

  //     setState(() {
  //       markerLatLng = null;
  //     });
  //   }
  // }

  void getContinuousLocationUpdates() async {
    Position position = await Geolocator()
        .getCurrentPosition(desiredAccuracy: LocationAccuracy.high);
    if (position != null && position.latitude != 0) {
      FirebaseDatabase.instance
          .reference()
          .child(Constant.USERS_KEY)
          .child(currentUser.uid)
          .update({
        Constant.LAT: position.latitude,
        Constant.LNG: position.longitude,
      });
    }
  }

  @override
  void dispose() {
    timer?.cancel();
    tripValues.cancel();
    super.dispose();
  }

  void setUserOfflineWhenCloseApp() async {
    await FirebaseDatabase.instance
        .reference()
        .child(Constant.USERS_KEY)
        .child(currentUser.uid)
        .child(Constant.IS_ONLINE_KEY)
        .onDisconnect()
        .set(false);
  }

  void changeUserStatus() async {
    Position position = await Geolocator()
        .getCurrentPosition(desiredAccuracy: LocationAccuracy.high);
    if (position.latitude == 0) {
      setState(() {
        isOnline = !isOnline;
      });
      return;
    }
    await FirebaseDatabase.instance
        .reference()
        .child(Constant.USERS_KEY)
        .child(currentUser.uid)
        .update({
      Constant.IS_ONLINE_KEY: isOnline,
    }).then((_) async {
      if (isOnline) {
        timer = Timer.periodic(
            Duration(seconds: 60), (Timer t) => getContinuousLocationUpdates());
        getContinuousLocationUpdates();
        pr.hide();
      } else {
        if (timer != null) timer.cancel();
        pr.hide();
      }
    }).catchError((onError) {
      pr.hide();
      setState(() {
        isOnline = !isOnline;
      });
    });
  }

  @override
  void initState() {
    super.initState();
    pr = new ProgressDialog(context,
        type: ProgressDialogType.Normal, isDismissible: false);
    initUser();
  }

  void getCurrentPosition() async {
    Position position = await Geolocator()
        .getCurrentPosition(desiredAccuracy: LocationAccuracy.high);
    if (position == null) {
      Toast.show(app.tr('please_access_location'), context);
      return;
    }
    moveToMyLocation(position);
  }

  void _showUserInfoInModalSheet() async {
    var name, phone, img;

    Response response = await Dio().post("${Constant.BASE_URL}select_user.php",
        queryParameters: {"firebase_key": userId});
    if (response.statusCode == 200 && response.data['status']) {
      var user = response.data['data'];
      setState(() {
        name = user[Constant.NAME_KEY];
        phone = user[Constant.PHONE_KEY];
        img = user[Constant.PHOTO_KEY];
      });
    }

    showModalBottomSheet(
        context: context,
        builder: (builder) {
          return name == null
              ? Center(
                  child: CircularProgressIndicator(),
                )
              : Container(
                  child: ListTile(
                    title: Text(
                      name,
                      style: TextStyle(fontSize: 20),
                    ),
                    subtitle: Text(
                      phone,
                      style: TextStyle(fontSize: 18),
                    ),
                    leading: CircleAvatar(
                      radius: 40,
                      backgroundImage: NetworkImage(img),
                    ),
                  ),
                );
        });
  }

  void moveToMyLocation(Position position) {
    myTarget = new LatLng(position.latitude, position.longitude);
    setState(() {
      _kGooglePlex = CameraPosition(
        target: myTarget,
        zoom: 14.4746,
      );
    });
  }

  @override
  Widget build(BuildContext context) {
    app = AppLocalizations.of(context);
    return Scaffold(
      appBar: AppBar(
        title: Text(app.tr('home_title')),
      ),
      drawer: Drawer(
        child: ListView(
          // Important: Remove any padding from the ListView.
          padding: EdgeInsets.zero,
          children: <Widget>[
            DrawerHeader(
              child: Image(
                width: 100,
                height: 100,
                image: AssetImage("images/logo.png"),
              ),
              decoration: BoxDecoration(
                color: Colors.white,
              ),
            ),
            ListTile(
              title: Text(app.tr('your_trips')),
              leading: Icon(Icons.history),
              onTap: () {
                Navigator.pop(context);
                Navigator.pushNamed(context, '/driver_trips');
              },
            ),
            ListTile(
              title: Text(app.tr('profile')),
              leading: Icon(Icons.person),
              onTap: () {
                Navigator.pop(context);
                Navigator.pushNamed(context, '/view_driver_profile');
              },
            ),
            ListTile(
              title: Text(app.tr('about_us')),
              leading: Icon(Icons.info),
              onTap: () {
                var url = Localizations.localeOf(context).languageCode == "ar"
                    ? "https://alfaraj.sa/elfarag/web_site/web_site/about_arabic.html"
                    : "https://alfaraj.sa/elfarag/web_site/web_site/about.html";
                Navigator.pop(context);
                Navigator.pushNamed(context, '/about', arguments: {"url": url});
              },
            ),
            ListTile(
              title: Text(app.tr('terms')),
              leading: Icon(Icons.info),
              onTap: () {
                var url = Localizations.localeOf(context).languageCode == "ar"
                    ? "https://alfaraj.sa/elfarag/web_site/web_site/terms_arabic.html"
                    : "https://alfaraj.sa/elfarag/web_site/web_site/terms.html";
                Navigator.pop(context);
                Navigator.pushNamed(context, '/terms', arguments: {"url": url});
              },
            ),
            ListTile(
              title: Text(app.tr('contact')),
              leading: Icon(Icons.contact_phone),
              onTap: () {
                Navigator.pop(context);
                Navigator.pushNamed(context, '/contact', arguments: {
                  "url":
                      "https://alfaraj.sa/elfarag/web_site/web_site/contact.html"
                });
              },
            ),
            ListTile(
              title: Text(app.tr('subscription')),
              leading: Icon(Icons.subscriptions),
              onTap: () async {
                var url =
                    "https://play.google.com/store/apps/details?id=com.wAlFaraj_8732233";
                Navigator.pop(context);
                if (await canLaunch(url)) {
                  await launch(url);
                } else {
                  throw 'Could not launch ';
                }
              },
            ),
            ListTile(
              title: Text(app.tr('share_app')),
              leading: Icon(Icons.share),
              onTap: () {
                Navigator.pop(context);
                Share.share(
                    'check farag app android : google.com , ios : apple.com');
              },
            ),
            ListTile(
              title: Text(app.tr('change_lang')),
              leading: Icon(Icons.language),
              onTap: () {
                Navigator.pop(context);

                this.setState(() {
                  Localizations.localeOf(context).languageCode == "ar"
                      ? MyApp.data.changeLocale(Locale("en", "US"))
                      : MyApp.data.changeLocale(Locale("ar", "AR"));
                });
              },
            ),
            ListTile(
              title: Text(app.tr('logout')),
              leading: Icon(Icons.subdirectory_arrow_left),
              onTap: () {
                Navigator.pop(context);
                if (haveTrip || isOnline) {
                  Toast.show(app.tr('convert_to_offline'), context);
                  return;
                }
                FirebaseAuth.instance.signOut();
                Navigator.of(context).pushNamedAndRemoveUntil(
                    '/splash', (Route<dynamic> route) => false);
              },
            ),
          ],
        ),
      ),
      body: Stack(
        children: <Widget>[
          Center(
            child: _kGooglePlex == null
                ? FlatButton(
                    child: Text(app.tr('get_location')),
                    onPressed: () {
                      getCurrentPosition();
                    },
                  )
                : isOnline
                    ? GoogleMap(
                        mapType: MapType.normal,
                        initialCameraPosition: _kGooglePlex,
                        myLocationEnabled: true,
                        markers: markerLatLng == null
                            ? null
                            : Set<Marker>.of(<Marker>[
                                Marker(
                                    markerId: MarkerId("1"),
                                    position: markerLatLng)
                              ]),
                      )
                    : Text(app.tr('your_are_offline')),
          ),
          haveTrip && isOnline
              ? Container(
                  margin: EdgeInsets.all(20),
                  padding: EdgeInsets.fromLTRB(12, 12, 8, 8),
                  color: Colors.white,
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: <Widget>[
                      Text(address),
                      Text(tripType),
                      Text(note),
                      Row(
                        children: <Widget>[
                          FlatButton(
                            child: Text(
                              agreeBtn,
                              style: TextStyle(color: Colors.green),
                            ),
                            onPressed: () {
                              // (trip[Constant.TRIP_TYPE_KEY] <= 3)
                              //     ? handleFirstType()
                              //     : handleSecondType();
                              handleFirstType();
                            },
                          ),
                          FlatButton(
                            child: Text(app.tr('cancel'),
                                style: TextStyle(color: Colors.red)),
                            onPressed: () async {
                              await FirebaseDatabase.instance
                                  .reference()
                                  .child(Constant.TRIPS_KEY)
                                  .child(currentTripId)
                                  .child(Constant.TRIP_STATUS)
                                  .set(TripStatus.cancelByDriver);

                              await FirebaseDatabase.instance
                                  .reference()
                                  .child(Constant.USERS_KEY)
                                  .child(userId)
                                  .child(Constant.CURRENT_TRIP_KEY)
                                  .remove();

                              await FirebaseDatabase.instance
                                  .reference()
                                  .child(Constant.USERS_KEY)
                                  .child(currentUser.uid)
                                  .child(Constant.CURRENT_TRIP_KEY)
                                  .remove();
                            },
                          ),
                        ],
                      ),
                      FlatButton(
                        child: Text(app.tr('view_user_info')),
                        onPressed: () {
                          _showUserInfoInModalSheet();
                        },
                      )
                    ],
                  ))
              : Container(),
          Align(
            alignment: Alignment.bottomCenter,
            child: (!isOnline || !haveTrip)
                ? Switch(
                    value: isOnline,
                    onChanged: (onval) {
                      if (_kGooglePlex == null) {
                        Toast.show(app.tr('acces_location_alert'), context);
                        return;
                      }

                      setState(() {
                        isOnline = onval;
                        pr.show();
                        changeUserStatus();
                      });
                    },
                  )
                : null,
          )
        ],
      ),
    );
  }
}
