import 'dart:async';

import 'package:dio/dio.dart';
import 'package:elfarag/utils/Constant.dart';
import 'package:elfarag/utils/UserData.dart';
import 'package:elfarag/utils/Utils.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/material.dart';

import 'driver/pending_account.dart';

class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  initState() {
    super.initState();
    loadData();
  }

  Future<Timer> loadData() async {
    return new Timer(Duration(seconds: 2), onDoneLoading);
  }

  onDoneLoading() async {
    // get user
    final FirebaseUser currentUser = await FirebaseAuth.instance.currentUser();
    if (currentUser == null ||
        currentUser.uid.isEmpty ||
        currentUser.phoneNumber.isEmpty) {
      Navigator.of(context)
          .pushNamedAndRemoveUntil('/login', (Route<dynamic> route) => false);
    } else {
      Response response = await Dio().post(
          "${Constant.BASE_URL}select_user.php",
          queryParameters: {"phone": currentUser.phoneNumber});
      if (response.statusCode == 200 && response.data['status']) {
        var user = response.data['data'];

        if (user == null) {
          Navigator.of(context).pushNamedAndRemoveUntil(
              '/user_type', (Route<dynamic> route) => false);
        } else if (user[Constant.BLOCK_KEY]) {
          Navigator.pushAndRemoveUntil(
              context,
              MaterialPageRoute(
                  builder: (context) => PendingAccount(
                        msg: Constant.BLOCK_ACCOUNT,
                      )),
              (Route<dynamic> route) => false);
        } else if (user[Constant.USER_TYPE_KEY] == Constant.USER_KEY) {
          // todo : in future check if this account block or not
          Navigator.of(context).pushNamedAndRemoveUntil(
              '/user_home', (Route<dynamic> route) => false);
        } else if (user[Constant.USER_TYPE_KEY] == Constant.DRIVER_KEY) {
          if (user[Constant.ACCESS_KEY]) {
            UserData.tripTypes = user[Constant.SERVICES_KEY];
            Navigator.of(context).pushNamedAndRemoveUntil(
                '/driver_home', (Route<dynamic> route) => false);
          } else {
            Navigator.pushAndRemoveUntil(
                context,
                MaterialPageRoute(
                    builder: (context) => PendingAccount(
                          msg: Constant.PENDING_ACCOUNT,
                        )),
                (Route<dynamic> route) => false);
          }
        } else {
          FirebaseAuth.instance.signOut();
          Navigator.of(context).pushNamedAndRemoveUntil(
              '/login', (Route<dynamic> route) => false);
        }
      } else {
        Utils.showMyDialog(context, "error", 'contact_us');
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        mainAxisSize: MainAxisSize.max,
        children: <Widget>[
          Image(
            image: AssetImage("images/logo.png"),
          ),
          Padding(
            padding: EdgeInsets.only(top: 20),
          ),
          CircularProgressIndicator()
        ],
      ),
    );
  }
}
