import 'package:dio/dio.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:elfarag/model/contact.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';

import 'package:toast/toast.dart';

class ContactUsScreen extends StatefulWidget {
  @override
  _ContactUsScreenState createState() => _ContactUsScreenState();
}

class _ContactUsScreenState extends State<ContactUsScreen> {
  final _formKey = GlobalKey<FormState>();
  String name, phone, msg;
  bool isLoading = false;

  @override
  void initState() {
    super.initState();
  }

  void sendData(var app) async {
    setState(() {
      isLoading = true;
    });
    var user = await FirebaseAuth.instance.currentUser();
    var item =
        ContactUs(name: name, phone: phone, msg: msg, createdBy: user.uid);
    Dio dio = new Dio();
    var response = await dio.post(
        "https://alfaraj.sa/elfarag/create_complaint.php",
        queryParameters: item.toJson());
    if (response.statusCode == 200) {
      if (response.data['status']) {
        Navigator.of(context).pop();
        Toast.show(app.tr('your_messgae_sent'), context);
      } else {
        setState(() {
          isLoading = false;
          Toast.show(app.tr('error'), context);
        });
      }
    } else {
      setState(() {
        isLoading = false;
        Toast.show(app.tr('error'), context);
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    var app = AppLocalizations.of(context);
    return Scaffold(
      appBar: AppBar(
        title: Text(app.tr('complaint')),
      ),
      body: SingleChildScrollView(
        child: Container(
            margin: EdgeInsets.all(20),
            child: Form(
              key: _formKey,
              child: Wrap(
                runSpacing: 20,
                children: <Widget>[
                  TextFormField(
                    validator: (String str) =>
                        str.isEmpty ? app.tr('invalid_input') : null,
                    onSaved: (String str) => name = str,
                    decoration: InputDecoration(labelText: app.tr('name')),
                  ),
                  TextFormField(
                    validator: (String str) =>
                        str.length < 5 ? app.tr('invalid_input') : null,
                    onSaved: (String str) => phone = str,
                    keyboardType: TextInputType.phone,
                    decoration: InputDecoration(labelText: app.tr('phone')),
                  ),
                  TextFormField(
                    keyboardType: TextInputType.text,
                    validator: (String str) =>
                        str.isEmpty ? app.tr('invalid_input') : null,
                    onSaved: (String str) => msg = str,
                    minLines: 2,
                    maxLines: 6,
                    decoration: InputDecoration(labelText: app.tr('details')),
                  ),
                ],
              ),
            )),
      ),
      floatingActionButton: isLoading
          ? CircularProgressIndicator()
          : FloatingActionButton.extended(
              onPressed: () {
                if (_formKey.currentState.validate()) {
                  _formKey.currentState.save();
                  sendData(app);
                }
              },
              icon: Icon(Icons.check),
              label: Text(app.tr('send')),
            ),
    );
  }
}
