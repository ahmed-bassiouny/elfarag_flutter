import 'package:easy_localization/easy_localization.dart';
import 'package:elfarag/main.dart';
import 'package:elfarag/utils/MyColor.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class UserTypeScreen extends StatefulWidget {
  @override
  _UserTypeScreenState createState() => _UserTypeScreenState();
}

class _UserTypeScreenState extends State<UserTypeScreen> {
  @override
  Widget build(BuildContext context) {
    var app = AppLocalizations.of(context);
    return Scaffold(
      backgroundColor: MyColor.green,
      body: Center(
          child: Column(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          FlatButton(
            onPressed: (){
              this.setState(() {
                  Localizations.localeOf(context).languageCode == "ar"
                      ? MyApp.data.changeLocale(Locale("en", "US"))
                      : MyApp.data.changeLocale(Locale("ar", "AR"));
                });
            },
            child: Text(app.tr('change_lang')),
          ),
          Text(app.tr('please_select_type'),
              style: TextStyle(fontSize: 25, color: Colors.white)),
          Padding(
            padding: EdgeInsets.fromLTRB(0, 50, 0, 0),
          ),
          Row(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              InkWell(
                onTap: (){
                  Navigator.pushNamed(context, '/complete_driver_profile');
                },
                child: Column(  
                children: <Widget>[
                  Container(
                    child: Image.asset("images/driver.png"),
                    height: 100,
                    width: 100,
                    padding: EdgeInsets.fromLTRB(0, 0, 0, 10),
                  ),
                  Text(
                    app.tr('driver'),
                    style: TextStyle(fontSize: 20, color: Colors.white),
                  )
                ],
              )),
              Padding(
                padding: EdgeInsets.fromLTRB(10, 0, 0, 0),
              ),
              InkWell(
                child: Column(
                  children: <Widget>[
                    Container(
                        child: Image.asset("images/man.png"),
                        height: 100,
                        width: 100,
                        padding: EdgeInsets.fromLTRB(0, 0, 0, 10)),
                    Text(
                      app.tr('user'),
                      style: TextStyle(fontSize: 20, color: Colors.white),
                    )
                  ],
                ),
                onTap: () {
                  Navigator.pushNamed(context, '/complete_user_profile');
                },
              )
            ],
          ),
          // Row(
          //   mainAxisSize: MainAxisSize.max,
          //   mainAxisAlignment: MainAxisAlignment.center,
          //   children: <Widget>[
          //     Center(
          //       child: Text("Driver"),
          //     ),
          //     Padding(
          //       padding: EdgeInsets.fromLTRB(10, 0, 0, 0),
          //     ),
          //     Center(
          //       child: Text("User"),
          //     ),
          //   ],
          // ),
        ],
      )),
    );
  }
}
