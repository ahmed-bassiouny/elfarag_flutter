
class ContactUs {
  String name;
  String phone;
  String createdBy;
  String msg;


  ContactUs({this.name, this.phone, this.createdBy, this.msg});

  Map<String, dynamic> toJson() =>
      {"name": name, "phone": phone, "created_by": createdBy, "message": msg};
}
